<?php
//Load includes
include_once "VisJS.php";
include_once "BinaryTree.class.php";
include_once "TreeGraph.php";
include_once "DatabaseWrapper.php";
include_once "BinaryTreeFactory.php";
include_once "DotGenerator.php";
include_once dirname(__FILE__)."/TreeMaker/FixedTreeLoader.php";
include_once 'Graphics.php';

require_once("config.php");

use BinaryTreeVisualizer\DB_Connector\PDO_UserDatabase as Database;
use BinaryTreeVisualizer\DB_Connector\UserRepository;
use BinaryTreeVisualizer\DB_Connector\DatabaseConfigVar as DefaultConfig;
use BinaryTreeVisualizer\DB_Connector\TestDatabaseConfig as TestConfig;
use BinaryTreeVisualizer\TreeGenerator\DefaultTreeGenerator as BTGen;
use BinaryTreeVisualizer\TreeGenerator\TreeConfig;
use BinaryTreeVisualizer\Graphics\VisJS;
use BinaryTreeVisualizer\TreeGenerator\FixedBinaryTreeLoader as TreeLoader;

use BinaryTreeVisualizer\Config;

interface VisualizerInterface{
    public function getVis();
    public function getTree();
}

class BTSubsystem{
    private $db, $rep, $vis;
    public function __construct(TreeConfig $treeConfig=null, $currentUserId=null){
        $this->db = new Database(DefaultConfig::quickCfg(Config::$db_config));
        $this->rep = new UserRepository($this->db, $currentUserId);
    }
    
    public function getDatabase(){
        return $this->db;
    }
    public function getRepository(){
        return $this->rep;
    }

}


/**
 * Quick Visualizer is an abstraction layer which uses default configuration
 * to provide access to the binary tree.
 *
 * UserModel->getName() is used as labels for this tree.
 **/
class QuickVisualizer implements VisualizerInterface{
    private $vis, $db, $dbconfig, $tree, $rep;
    public function __construct(TreeConfig $treeConfig=null){
        $this->db = new Database(DefaultConfig::quickCfg(Config::$db_config));
        //$btGenerator = new BTGen($this->db);
        $btGenerator = new TreeLoader($this->db);
        
        $this->tree = $btGenerator->makeTree($treeConfig);
        $this->vis = new VisJS($this->tree);
        $this->vis->setObjectLabel('getName');
        $this->rep = new UserRepository($this->db);
    }

    public function getVis(){
        return $this->vis;
    }

    public function getTree(){
        return $this->tree;
    }

    public function getRepository(){
        return $this->rep;
    }
    
    public static function getHeadLinks(){
        $links = "";
        $links .= VisJS::inc_head();
        return $links;
    }

    //TODO FIXME
    public function regenTree(TreeConfig $config){
        $this->tree = null;
        $this->tree = new BTGen($this->db);
        $this->tree->makeTree($config);
        $this->update();
    }

    private function update(){
        $this->vis = new VisJS($this->tree);
    }

    public function getDatabase(){
        return $this->db;
    }
}

?>