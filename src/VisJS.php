<?php

namespace BinaryTreeVisualizer\Graphics;

include_once 'Graphics.php';
include_once 'BinaryTree.class.php';
include_once "TreeGraph.php";

use BinaryTreeVisualizer\BinaryTree;
use BinaryTreeVisualizer\TreeGraph;
use BinaryTreeVisualizer\Node;

use BinaryTreeVisualizer\DB_Connector\Models\UserModel;
/**
 * Interface callback
 **/
/*
  interface DisplayNode{
    public void DisplayCallback(
  }
 */

class VisJS implements JSGraphicsScript{
    private $containerId = 'container', $newMemberBoxId = "avail-member-box",
        $contextBoxId = 'member-box', $infoBoxId = 'info-box';
    //container, a div container which contains the canvas
    // new member box. The div element when the user clicks an available member.
    //member box is displayed when a user clicks a member in the tree. 
    // Info box is displayed when a user clicks a non sponsored member. This should be the fallout item for member box, if no member box is defined.
    private $tree; 
    private $labelKey;
    private $accessType;
    private $hoverText;

    private $nodeDatatype;    
    private $subText;

    /** 
     * The results from this method is only used if the $subText variable is not set.
     * Root must be set before calling this function.
     * This methods performs the following tests on the root node before deciding the method to retrieve the graph label.
     *  The function firsts check whether the param is an property of the method.
     * If a property with the name is not found tests will be done,
     *
     * @param String containing the field/method name from the class on
     * Example: "getName()" or "$->name".
     **/
    public function setObjectLabel($param){
        $test = $this->tree->getRoot()->getData();
        //$datatype = $this->getNodeDatatype();
        $datatype = gettype($test);
        $valid = false;
        
        
        if($datatype=="object"){
            //Check if param method exist's in object
            if( method_exists($test, $param) ){
                $valid = true;
                $this->accessType = "method";
            }
            //Check if param property exists in object
            if( property_exists($test, $param) ){
                $valid = true;
                $this->accessType = "param";
            }
        }

        
        if($valid)
            $this->labelKey = $param;
    }

    /**
     * Return the datatype of the node
     **/
    private function getNodeDatatype(){
        $test = $this->tree->getRoot()->getData();
        $this->nodeDatatype = gettype($test);
    }
    
    public function __construct(BinaryTree $tree){
        $this->tree = $tree;
        $this->getNodeDatatype();
    }
    
    public function getHeader()
    {
        return 
        '     
        <script src="graph-engine/visjs/vis.js"></script>
        <link href="graph-engine/visjs/vis.css" rel="stylesheet" type="text/css" /> ';
    }

    public static function inc_head($prefix = ""){
        return  <<<EOL
         <script src="{$prefix}btvis/graph-engine/visjs/vis.js"></script>
        <link href="{$prefix}btvis/graph-engine/visjs/vis.css" rel="stylesheet" type="text/css" /> 
EOL;

    }

    public function setContainer($domID)
    {
        $this->containerId = $domID;
    }

    public function getContainerID(){
        return $this->containerId;
    }

    private function getNodeTitle(Node $node){
        $data = 'NULL';
        $labelkey = $this->labelKey;
        
        if($this->nodeDatatype === "string")
            $data = $node->getData();
        
        if($labelkey){
            if($this->accessType === "method")
                $data = $node->getData()->getName();
            else
                $data = $node->getData()->${$labelkey};
        }
        
        if(!gettype($data))
            return "Error Occured!";    //Log: A string was not returned.
           
        return $data; 
    }

    private function getNodeLabel(Node $node ){
        $data = 'NULL';
        $labelkey = $this->labelKey;
        
        if($this->nodeDatatype === "string")
            $data = $node->getData();
        
        if($labelkey){
            if($this->accessType === "method")
                $data = $node->getData()->{$labelkey}();
            else
                $data = $node->getData()->${$labelkey};
        }
        
        if(!gettype($data))
            return "Error Occured!";    //Log: A string was not returned.
           
        return $data; 
    }

    public function getScript()
    {
        $nodes = "";
        $edges = "";
        $nodeShape = "box";
        $containerId = $this->containerId;
        $newMemberBoxId =  $this->newMemberBoxId;
        $contextBoxId = $this->contextBoxId;
        $infoBoxId  = $this->infoBoxId;
        
        $treeGraph = TreeGraph::create($this->tree);
        $rawNodes = $treeGraph->getNodes();
        $rawEdges = $treeGraph->getEdges();

        //        $hoverText = "${name}; //for test purpose only
        $nid = 0;
        $info = '';
        $clusterId = 0;
        $pClusterId = 1;
        $n = 0;
        
        $rootNode = null;
        //$nodeId = 1;
        foreach($rawNodes as $rawNode)
        {

            //$nodeId = $treeGraph->getNodeId($rawNode)+1;
            $nodeId = $rawNode->getData()->getTreePos();
            //$nodeId = $rootNode->getData()->getTreeId();
            if($rootNode == null)//Select the first node as the root 
                $rootNode = $rawNode;

            
            $name = $this->getNodeTitle($rawNode);

            $owned = false;
            if ($rawNode->getData() instanceof UserModel){
                $owner_id = $rawNode->getData()->getEnrollerId();
                $name = $rawNode->getData()->getName();
                $id = $rawNode->getData()->getID();
                $email = $rawNode->getData()->getEmail();
                $parentPos = $rawNode->getData()->getEnrollerID();
                $contact  = $rawNode->getData()->getNumber();
                $token = $rawNode->getData()->getToken();
                $sponsor = $rawNode->getData()->getSponsorCount();
                $alias = $rawNode->getData()->getAlias();
                $info = ", info:{id:'{$id}', name:'{$name}', email:'{$email}', parent:'{$parentPos}', contact:'{$contact}', token:'{$token}', sponsor: '{$sponsor}', alias:'{$alias}'}";
                if($rootNode->getData()->getBTId() === $owner_id)
                    $owned=true;
                //echo $nodeId . "=>". $rawNode->getData()->getName(). "</br>";
            }

            $clusterId = 0;
            if($rawNode->getData() instanceof UserModel){
                $pos = $rawNode->getData()->getTreePos();
                $lvl = floor(log($pos, 2)+0.01);  //added 0.01 to fix bug
                $first = pow(2, $lvl);
                
                if($lvl > 0){
                    $last  = pow(2, $lvl+1) - 1;
                    $mid = ($last+1)-($first);
                    $mid = $mid/2;
                    $mid = $mid + $first;
                    
                    if ($pos < ($mid))
                        $clusterId = 2;
                    else
                        $clusterId = 1;

                    //echo " {$pos}:({$first}, {$last}) - {$mid }- LVL(".$lvl .") [{$clusterId}] \n </br>";
                }
            }

            
            if( $name === "null"){
                //$nud = $rawNode->getData()->getTreePos();
                $nodes .= "{ id:". $nodeId. ", label:'Available', group: 'empty', title:'{$name}'
 ,color:'white' , align: '{$rawNode->getData()->getBTId()}' , cid: {$clusterId} "  . "},\n\t ";
            
            }elseif($owned){
                $nodes .= "{ id:". $nodeId. ", label:'".$this->getNodeLabel($rawNode). "', title:'{$name}'
 ,group:'ownedGroup' {$info} , cid: {$clusterId}"  . "},\n\t ";

                       //"{ id:". $treeGraph->getNodeId($rawNode). ", label:'Available', shape:'{$nodeShape}' , title:'{$name}' ,color:'green' "  . "},\n\t ";
            }else{
                //$nodes .= "{ id:". $treeGraph->getNodeId($rawNode). ", label:'".$this->getNodeLabel($rawNode). "', shape:'{$nodeShape}' , title:'{$name}' ,color:'red' "  . "},\n\t ";
                $nodes .= "{ id:". $nodeId. ", label:'".$this->getNodeLabel($rawNode). "',  title:'{$name}', group:'userGroup' {$info} , cid: {$clusterId} "  . "},\n\t ";
            }

            $nid ++;
        }
        $parentMap = "var parentMap=[];";
        

        /*foreach($rawEdges as $rawEdge)
        {
            $adjNodes = array($rawNodes[$rawEdge[0]], $rawNodes[$rawEdge[1]] );
            $edges .= "{ from: ". ($treeGraph->getNodeId($adjNodes[0])+1)
                   . ", to: ".      ($treeGraph->getNodeId($adjNodes[1])+1) ." "
                   . " , smooth: {type: 'discrete'},  
                       color: 'grey', " 
                   . "}, \n\t";                       
            //            echo $adjNodes[0]->getName(). ", " . $adjNodes[1]->getName(). "\n";
            }*/

        foreach($rawEdges as $rawEdge){
            $parent_ = $rawEdge[0];
            $child_  = $rawEdge[1];
            $from  = $rawNodes[$parent_]->getData()->getTreePos();
            $to = $rawNodes[$child_]->getData()->getTreePos();
            $edges .= "{from: '{$from}', to: '{$to}', smooth: {type: 'discrete'}, color: 'grey' },\n\t";
            
        }
        /*        for($i=1; $i < count($rawNodes); $i++){
                //$from = $rawNodes[$rawEdge];
            $from = $i;
            $to = $i*2;            
            if(isset($rawNodes[$to]))
                $edges .= "{from: '{$from}', to: '{$to}', smooth: {type: 'discrete'}, color: 'grey' },\n\t";
            $b = isset($rawNodes[$to]);
            echo " ({$from},  {$to}) :{$b}<br />";
            $to = ($i*2)+1;
            if(isset($rawNodes[$to]))
                $edges .= "{from: '{$from}', to: '{$to}', smooth: {type: 'discrete'}, color: 'grey' },\n\t";
            $b = isset($rawNodes[$to]);
            echo " ({$from},  {$to}) : {$b} <br/>";

            }*/
        
                $script = <<<EOL
      var options = {
         autoResize: true,
         height: '100%',
         width: '100%',
         groups: {
/*            userGroup:
               {
                  color: {background:'red'}, border:'3', font:{color: 'white'} 
               },*/
           /* ownedGroup:
               { color: {background: 'blue'}, border: '2', font:{color: 'white'} }
            ,
            empty:
               {
                  color: {background:'white', border:'3'}
               }
            ,*/

           empty: {
                shape: 'icon',
                icon: {
                    face: 'FontAwesome',
                    code: '\uf007',
                    size: 50,
                    color: 'white'
                }
            },
            ownedGroup: {
                shape: 'icon',
                icon: {
                    face: 'FontAwesome',
                    code: '\uf007',
                    size: 50,
                    color: 'blue'
                }
            },
   
            userGroup: {
                shape: 'icon',
                icon: {
                    face: 'FontAwesome',
                    code: '\uf007',
                    size: 50,
                    color: 'orange'
                }
            },
            userCluster: {
                shape: 'icon',
                icon: {
                    face: 'FontAwesome',
                    code: '\uf0c0',
                    size: 50,
                    color: 'orange'
                }
            }


         },

         physics: false,
               nodes: {
                fixed: {
                     x:true,
                     y:true
                 }
               },

         layout: {
           randomSeed: 0,
           improvedLayout:false,
           hierarchical: {
           enabled:true,
           levelSeparation: 150,
           nodeSpacing: 100,
           treeSpacing: 200,
           blockShifting: false,
           edgeMinimization: false,
           parentCentralization: true,
           direction: 'UD',        // UD, DU, LR, RL
           sortMethod: 'directed'   // hubsize, directed
         }
  },
        interaction: {
              dragNodes: false,
              dragView:  true,
              zoomView:  true,
              hover:     false,
              keyboard: true,
                navigationButtons: true
        },
      };

      var nodes= new vis.DataSet([
          {$nodes}
      ]);
      
      var edges= new vis.DataSet([
         {$edges}
      ]);

      var container = document.getElementById("{$containerId}");
      var nmBox     = document.getElementById("{$newMemberBoxId}");
      var contextBox   = document.getElementById("{$contextBoxId}");
      var infoBox    = document.getElementById("{$infoBoxId}");

      var data = {
         nodes : nodes,
         edges : edges,
      };

      var graph = new vis.Network(container, data, options);

     var x=0, y=0, displayInfo=false, context=null;
     var lastNode = null;    
     var clickRect = null;
     var lastClick = null;
     var lastNodePos   = null;

     graph.on("selectNode", function (params) {
      //console.log(params);
      if (params.nodes.length == 1) {
          if (graph.isCluster(params.nodes[0]) == true) {
              graph.openCluster(params.nodes[0]);
          }
          selectNodeId = params.nodes[0];
          graph.focus(selectNodeId);
          pos = graph.canvasToDOM(graph.getPositions()[selectNodeId]);
          lastNodePos = pos;
          if(nmBox){
            nmBox.style.left =  pos.x+"px";
            nmBox.style.top = pos.y+"px";
            //console.log("moveBox: "+ pos.x);
           }      
          if(infoBox){
            infoBox.style.left = pos.x+20+"px";
            infoBox.style.right = pos.y+20+"px";
          }     
      }

      
     });

     graph.on("click", function (params) {
       var node = params['nodes'][0];
       displayInfo=false;
       if(node!=null){
        //console.log('node:'+ params['nodes'][0]);
        if(lastNodePos){
           cPos = graph.DOMtoCanvas({x: lastNodePos.x, y: lastNodePos.y});
           x = cPos.x;
           y = cPos.y;
        }else{
         x = params["pointer"]['canvas']['x'];
         y = params["pointer"]['canvas']['y'];   
        }
         displayInfo=true;
         lastNode = nodes.get(node);
         lastClick = params['pointer'];

         //console.log(params);	
		 
         if(infoBox.onDisplay){
                infoBox.onDisplay(lastNode);
         }
       }
     });
	 
	 
	 
	 

//This is a sample. Let the designer use html to display in final product.
     graph.on("afterDrawing", function(ctx)  {
   
    if(nmBox) nmBox.style.visibility = "hidden";
    if(infoBox) infoBox.style.visibility = "hidden";

       if(displayInfo && lastNode.info){
        if(infoBox){
           infoBox.style.visibility="visible";
         
        }else{
         var fs = ctx.fillStyle;
         ctx.fillStyle = "#4a4646";
         ctx.fillRect(x,y, 180,100);
         
         ctx.fillStyle="white";
         ctx.textAlign="left";
         var name= lastNode.info.name,
             email=lastNode.info.email, enroller=lastNode.info.parent;                 
         ctx.fillText("Name: "+ name, x+10,y+20);
         ctx.fillText("Email:" + email, x+10,y+40);
         ctx.fillText("Enroller: "+ enroller, x+10,y+60);
   //    ctx.stroke(); 
         ctx.fillStyle = fs;
         context = ctx;
        }
       }


      if(displayInfo && lastNode.title === "null"){

       if(nmBox){
          nmBox.style.visibility="visible";
          //nmBox.style.left = lastClick["DOM"]['x']+"px";
          //nmBox.style.top = lastClick["DOM"]['y']+"px";  
          nmBox.setAttribute("nodeId", lastNode.id);
          nmBox.focus();
          //$('input')[0].focus();
        }else{      
         var fs = ctx.fillStyle;
         ctx.fillStyle = "#4a4646";
         ctx.fillRect(x,y, 180,70);
         
         ctx.fillStyle="white";
         ctx.textAlign="left";
         ctx.fillText("Add Node", x+10,y+20);

         ctx.addEventListener();
         ctx.fillStyle = fs;
         context = ctx;
         context.canvas.addEventListener("click", on_click, false);          
         
         clickRect = [lastClick['DOM']['x'],lastClick['DOM']['y'], 180/2, 30];
            }
       }

      

     });

var evnt = null;
   function on_click(e){
/*     evnt = e;
     //mousePos = getMousePos(context.canvas, e);
     var x = lastClick['canvas']['x'], y = lastClick['canvas']['y'];
     var x = e.clientX, y = e.clientY;
	 alert("Clicked!");
     if (x > clickRect[0] && x <= clickRect[0]+clickRect[2])
      if(y > clickRect[1] && y <= clickRect[1]+clickRect[3])
        alert("Add new member.");
  */  
//     console.log("clicked! "+lastClick['canvas']['x']+" ->"+ clickRect[0]); 
     context.canvas.removeEventListener("click", on_click, false);
   }

    //var graph = new vis.Network(container, data, {});
/*      graph.setOptions{ 
           { 
           physics: {enabled:false}
           }
      };*/
       if(nmBox)
         document.getElementById("{$containerId}").firstChild.appendChild(nmBox);
       if(infoBox)
         document.getElementById("{$containerId}").firstChild.appendChild(infoBox);



//----BEGIN CLUSTERING CODE---
var clusterIndex = 0;
var clusters = [];
var lastClusterZoomLevel = 0;
var clusterFactor = 0.8;


graph.on('zoom', function(params){
       displayInfo=false;
  //console.log("bleh", params);
  if(params.scale < lastClusterZoomLevel*clusterFactor){
        // console.log("bleh", params);
  
      makeClusters(params.scale);
      lastClusterZoomLevel = params.scale;
    graph.setOptions(options);
  }else{
    openClusters(params.scale);
  }
});

graph.once('initRedraw', function() {
    if (lastClusterZoomLevel === 0) {             
            lastClusterZoomLevel = graph.getScale();
        }
});

function makeClusters(scale){
  /*var clusterOptionsByData = {
            processProperties: function (clusterOptions, childNodes) {
                clusterIndex = clusterIndex + 1;
                var childrenCount = 0;
                for (var i = 0; i < childNodes.length; i++) {
                    childrenCount += childNodes[i].childrenCount || 1;
                }
                clusterOptions.childrenCount = childrenCount;
                clusterOptions.label = "# " + childrenCount + "";
                clusterOptions.font = {size: childrenCount*5+30}
                clusterOptions.id = 'Total Members:' + clusterIndex;
                clusters.push({id:'Total Members:' + clusterIndex, scale:scale});
                return clusterOptions;
            },
            clusterNodeProperties: {borderWidth: 3, group:'userCluster', font: {size: 30}}
        }
  graph.clusterOutliers(clusterOptionsByData);*/

  var clusterOptions1 = {
     processProperties: function(clusterOptions, childNodes){
          clusterIndex = clusterIndex + 1;
          var childrenCount = 0;
          for(var i=0; i< childNodes.length; i++)
           {
                childrenCount += childNodes[i].childrenCount || 1;
           }
           clusterOptions.childrenCount = childrenCount;
           clusterOptions.label = "Total Members: "+ childrenCount +" Person";
           clusterOptions.id = 'Total Members:'+clusterIndex;
           clusters.push({id:'Total Members:' + clusterIndex, scale:scale});
          return clusterOptions;
     },
     joinCondition: function(nodeOptions){
      //return (nodeOptions.id % 2 == 1;
      return nodeOptions.cid == 1;
      return (Math.floor((Math.log2(nodeOptions.id))) ) > 2;
     } ,
   clusterNodeProperties: {borderWidth: 3, group:'userCluster', font: {size: 30}}
  };


  var clusterOptions2 = {
     processProperties: function(clusterOptions, childNodes){
          clusterIndex = clusterIndex + 1;
          var childrenCount = 0;
          for(var i=0; i< childNodes.length; i++)
           {
                childrenCount += childNodes[i].childrenCount || 1;
           }
           clusterOptions.childrenCount = childrenCount;
           clusterOptions.label = "Total Members: "+ childrenCount +" Person";
           clusterOptions.id = 'Total Members:'+clusterIndex;
           clusters.push({id:'Total Members:' + clusterIndex, scale:scale});
          return clusterOptions;
     },
     joinCondition: function(nodeOptions){
      //return (nodeOptions.id % 2 == 1;
      return nodeOptions.cid == 2;
      return (Math.floor((Math.log2(nodeOptions.id))) ) > 2;
     } ,
   clusterNodeProperties: {borderWidth: 3, group:'userCluster', font: {size: 30}}
  };

  graph.clustering.cluster(clusterOptions1);
  graph.clustering.cluster(clusterOptions2);
   
/*
  var clusterOptionsByData = {
            processProperties: function (clusterOptions, childNodes) {
                clusterIndex = clusterIndex + 1;
                var childrenCount = 0;
                for (var i = 0; i < childNodes.length; i++) {
                    childrenCount += childNodes[i].childrenCount || 1;
                }
                clusterOptions.childrenCount = childrenCount;
                clusterOptions.label = "# " + childrenCount + "";
                clusterOptions.font = {size: childrenCount*5+30}
                clusterOptions.id = 'cluster:' + clusterIndex;
                clusters.push({id:'cluster:' + clusterIndex, scale:scale});
                return clusterOptions;
            },
            clusterNodeProperties: {borderWidth: 3, group:'userCluster', font: {size: 30}}
        }*/
//  graph.cluster.(clusterOptionsByData);

  //
  //graph.stabilize();
  graph.setOptions(options);
}

function openClusters(scale){
  var newClusters = [];
        var declustered = false;
        for (var i = 0; i < clusters.length; i++) {
            if (clusters[i].scale < scale) {
                graph.openCluster(clusters[i].id);
                lastClusterZoomLevel = scale;
                declustered = true;
            }
            else {
                newClusters.push(clusters[i])
            }
        }
        clusters = newClusters;
        /*if (declustered === true && document.getElementById('stabilizeCheckbox').checked === true) {
            // since we use the scale as a unique identifier, we do NOT want to fit after the stabilization
            //graph.setOptions({physics:{stabilization:{fit: false}}});
            //graph.stabilize();
        }*/
      //graph.setOptions(options);
}
//--END OF CLUSTERING CODE --

EOL;

                $script .= $parentMap;
        return "<script type=\"text/javascript\">\n" . $script . "\n</script>";
    }

    /** Use this only for test purpose **/
    public function getHTML(){
        $head = $this->getHeader();
        $cont = $this->getContainer();
        $script = $this->getScript();
        $html = <<<EOL
        <!doctype>
         <html>
           <head> 
             <title>Graph Test</title>
               {$head}
            </head>

           <body>
              {$cont}
              {$script}
           <body>
        </html>
EOL;
        return $html;
    }

    public function getContainer(){
        return "<div id=\"{$this->containerId}\" ></div>";
    }
        
}

?>