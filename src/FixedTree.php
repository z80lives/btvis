<?php
namespace BinaryTreeVisualizer\TreeGenerator;


require_once 'BTVisualizer.php';
require_once 'BinaryTreeFactory.php';

use BinaryTreeVisualizer\DB_Connector\UserRepository as Repository;
use BinaryTreeVisualizer\DB_Connector\GenericDatabase as Database;
use BinaryTreeVisualizer\BinaryTree;
use BinaryTreeVisualizer\BinaryTreeNode;
use BinaryTreeVisualizer\BackTracker;

require_once 'DotGenerator.php';
//use BinaryTreeVisualizer\BTMarkupSpitter\DotConverter;
use BinaryTreeVisualizer\DotConverter;

class FixedTreeGenerator implements TreeGenerator{
    private $db;
    private $rep;
    private $config;

    private $usrIds;

    public function __construct(Database $db){
        $this->db = $db;
        $this->rep = new Repository($db);
    }

    /**
     * Generate a fixed binary tree. Refer to the documentation for 
     * details.
     **/
    public function makeTree($config = null){
        $tree = null;
        $config = $this->prepareConfig($config);
        $rep = $this->rep;

        if(gettype($config) == "string"){
            
        }
        
        $rootUserId = $rep->getUserID($config->getRoot());
        $rootUser = $rep->getUser($rootUserId);

        //echo $rootUserId;

        $tree = BinaryTree::withRoot(BinaryTreeNode::withData($rootUser));
        $node = $tree->getRoot();
        $nodePos = $config->fixedPosRoot;
        
        $backTracker = new BackTracker();

        $walkFunc = (function($node, $parent, $level, $index) use ($rep, $config,$nodePos){

                $BTId = $node->getData()->getBTId();
                $BTId = ltrim($BTId, $config->prefix);
                $nodePos = (int) $BTId;
                
            $leftNodeId = $rep->getUserId($config->prefix.($nodePos*2));
            $rightNodeId = $rep->getUserId($config->prefix.($nodePos*2+1));
            if($leftNodeId){
                $leftUser = $rep->getUser($leftNodeId);
                $node->addChild(BinaryTreeNode::withData($leftUser), BinaryTreeNode::LEFT);
            }
            if($rightNodeId){
                $rightUser = $rep->getUser($rightNodeId);
                $node->addChild(BinaryTreeNode::withData($rightUser), BinaryTreeNode::RIGHT);
            }
                
        });
        
        $backTracker->setMethod('enter', $walkFunc);
        $backTracker->executeWalk($node);

        //Display output in dot format
        //$dot = new DotConverter($tree);
        //echo $dot->getString();
        //$graph = new Graph();
        return $tree;        
    }

    /**
     * Use default configuration, if config variable is missing.
     **/
    private function prepareConfig($config)
    {
        if($config == null){
            $config = new TreeConfig();
        }
        $this->config = $config;
        return $config;
    }
}


?>