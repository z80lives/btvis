<?php

namespace BinaryTreeVisualizer\TreeGenerator;


//require_once 'BTVisualizer.php';
require_once dirname(__FILE__).'/../BinaryTreeFactory.php';
require_once dirname(__FILE__).'/../UserModel.class.php';
use BinaryTreeVisualizer\DB_Connector\UserRepository as Repository;
use BinaryTreeVisualizer\DB_Connector\GenericDatabase as Database;
use BinaryTreeVisualizer\BinaryTree;
use BinaryTreeVisualizer\BinaryTreeNode;
use BinaryTreeVisualizer\BackTracker;
use BinaryTreeVisualizer\DB_Connector\Models\UserModel;

require_once dirname(__FILE__).'/../DotGenerator.php';
//use BinaryTreeVisualizer\BTMarkupSpitter\DotConverter;
use BinaryTreeVisualizer\DotConverter;


class FixedBinaryTreeLoader implements TreeGenerator{
    private $db, $rep, $config, $usrIds;

    public function __construct(Database $db)
    {
        $this->db = $db;
        $this->rep = new Repository($db);
    }


    public function makeTree($config = null)
    {
        $tree = null;
        $config = $this->prepareConfig($config);
        $rep = $this->rep;
        if($config->rootUsrId == null){
            throw new \Exception("Fixed Tree Loader: Please set a master user ID before constructing a tree");
            return null;
        }
        $usrId = $config->rootUsrId;
        $user = $rep->getUser($usrId);
        if($user == null)
        {
            throw new \Exception("Fixed Tree Loader: User with id {$usrId} not found!"); 
        }
        $treeId = $rep->getTreeId($user);
        $map = $rep->getUserMap($treeId);

        //echo "POS = ". var_dump($rep->getPositionId($usrId, $treeId));

        //add root person
        $tree = BinaryTree::withRoot(BinaryTreeNode::withData($user));
        $pos = $rep->getPositionid($usrId, $treeId);

        $lpos = $pos;
        $walkFunc = (function($node, $parent, $lvl, $index) use($rep, $config, $lpos, $map, $treeId){
                //$id = $rep->getPositionId($usr
                //Check for left child
                $usrId = $node->getData()->getID();
                $data = $node->getData();
                $lpos = $rep->getPositionID($usrId, $treeId);
                
                if (isset($map[$lpos*2])){
                    //Found left
                    $user = $rep->getUser($map[$lpos*2]);
                    $node->addChild(BinaryTreeNode::withData($user),
                                    BinaryTreeNode::LEFT);
                }
                
                if (isset($map[$lpos*2+1])){
                    //Found right
                    $user = $rep->getUser($map[$lpos*2+1]);
                    $node->addChild(BinaryTreeNode::withData($user),
                                    BinaryTreeNode::RIGHT);
                }

            });
        
        $exitFunc = (function($node, $parent, $lvl) {
            $children = $node->getChildren();
            $leftChild = $children[0];
            $rightChild = $children[1];
            $data = $node->getData();

            //            echo 
            if($leftChild == null){
                $nullUser = new UserModel( ($data->getID()),
                                               "null",
                                               "as@email.com",
                                                "L");                 
                    $node->addChild(BinaryTreeNode::withData($nullUser),                                                             BinaryTreeNode::LEFT
                    );
                    if($data)
                        $nullUser->setTreePos($data->getTreePos()*2);
            }
            if($rightChild == null){
                $nullUser = new UserModel( $data->getID(),
                                               "null",
                                               "as@email.com",
                                               "R");                 
                    $node->addChild(BinaryTreeNode::withData($nullUser),                                                             BinaryTreeNode::RIGHT
                    );
                    if($data)
                        $nullUser->setTreePos(($data->getTreePos()*2)+1);
            }
        });

        
        $node = $tree->getRoot();
        
        $bt = new BackTracker();
        $bt->setMethod('enter', $walkFunc);
        $bt->setMethod('exit', $exitFunc);
        $bt->executeWalk($node);
        
        return $tree;
    }

    /**
     * Use default configuration, if config variable is missing.
     **/
    private function prepareConfig($config)
    {
        if($config == null){
            $config = new TreeConfig();
        }
        $config->fixedTreeMode = true;        
        $this->config = $config;
        return $config;
    }

}


?>