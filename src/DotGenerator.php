<?php
/**
 * Dot Generator can be used to convert the tree into a readable markup format.
 * Dot is graph description language used in GraphViz software package. Dot format is 
 * also supported by the VisJS module. Use this module to generate tree for test purposes.
 **/
namespace BinaryTreeVisualizer;

include_once 'UserModel.class.php';
use BinaryTreeVisualizer\DB_Connector\Models\UserModel;

/**
 * Adapter interface for a binary tree markup generator.
 * @todo use this in future updates.
 **/
interface BTMarkupSpitter{
    public function read(BinaryTree $tree);
    public function getString();
}

/**
 * Generate DOT markup from a binary tree.
 *
 **/
class DotConverter implements BTMarkupSpitter{
    private $tree;
    private $outString="";

    /** Default constructor. 
     * Prepare and Generate the Markup Script.
     * @param BinaryTree $tree This parameter is passed into read() method once called.
     **/
    public function __construct(BinaryTree $tree){ $this->read($tree); }

    /**
     * Read a binary tree 
     *
     * @param BinaryTree $tree read and parse the binary tree string.
     **/
    public function read(BinaryTree $tree){
        $root = $tree->getRoot();
        $out = "//BT Visualizer Output  \n";
        $out .= "digraph bt { \n";
        $out .= $this->parseNode($root);
        $out .= "}";
        
        $this->outString = $out;
    }

    /**
     * Fetch output.
     * @return String the output in string format.
     **/
    public function getString(){
        return $this->outString;
    }

    /**
     * Use depth-first recursion to parse the tree and construct the string from it.
     *
     * @param BinaryTreeNode $node Node to read from.
     * @lvl Height of the binary tree. Root is assumed to be 1.
     **/
    private function parseNode(BinaryTreeNode $node, $lvl=1){
        $label = '';
        $out = "";
        $data = $node->getData();
        //echo var_dump($data );
       
        if($data instanceof UserModel)
            $label = $data->getName();
        elseif (gettype($data) === 'string')
            $label = $data;

        if($lvl){
            for($i=0; $i<$lvl; $i++)
                $out .= " ";
        }
        $out .= $label;
        
        $child = $node->getChildren();

        if ($child[0] || $child[1]){
            $out .= " -> ";
            if (sizeof($child)==2) $out .= " {";

            for($i=0; $i<=1; $i++)
                if($child[$i])
                    $out .= ' '. $child[$i]->getData()->getName() .' ';
            
            
            if (sizeof($child)==2) $out .= " }";
        }
        
        $out .= ";\n";

        for($i=0; $i<=1; $i++)
            if($child[$i]){
                $out .= $this->parseNode($child[$i], $lvl+1);
            }
        return $out;
    }
}

?>