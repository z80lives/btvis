<?php
/** 
 * Database Connecter
 *  Wraps database code 
 *
 *  These classes acts as an interface between BinaryTree Visualizer code 
 * and the main code. Remove or refactor code to work with the implementation,
 * for maximum efficency 
 * @author shath ibrahim <shath.ibrahim@gmail.com>
 **/
namespace BinaryTreeVisualizer\DB_Connector;

include_once 'BinaryTree.class.php';
include_once 'UserModel.class.php' ;

use BinaryTreeVisualizer\DB_Connector\Models\UserModel as UserModel;

use PDO;

/**
 * 
 **/
interface GenericDatabase{
    public function success();
    public function results();
    public function query($query);
}

////Deprecated
//interface GenericTable{
//  public function retrieveRows($keys);
//  public function addRow(GenericRow $row);
//}

/**
 * Repository methods for our project's pre-designed user table.
 **/
interface UserModelMethods{
    //public function init(GenericDatabase $db);
    public function getUserID($binTreeID);
    /** <Get binary tree position of the user> **/
    public function getPositionID($usrID);
    public function getUserMap();  //Fetch all available user => position mappings, in the tree.
    public function getUser($userID);
    
    /** Count user's in User table**/
    public function countUsers();
    public function getAllUserIds();
    public function getChildren($usrID);
}

interface BinaryTreeMethods{
    /**
     * Creates a new user and returns their tree id.
     **/
    public function createNewRootUser(UserModel $user);

    /**
     * Delete root user of a tree
     **/
    public function deleteRootUser(UserModel $user);
    
    /**
     * Gets last active tree
     **/
    public function getLastTreeIndex();

    /**
     * Adds a user to a tree
     **/
    public function addUserToTree(UserModel $user, UserModel $sponsor, UserModel $parent, UserModel $master = null, $dir = null);

    /**
     * Removes a user from the tree
     **/
    public function removeUser(UserModel $user, UserModel $treeMaster);

}


/**
 * UserRepository class provides read access to the User table.
 * UserRepository class is used by the Binary tree creator to access the database. 
 *  The code currently depends on two tables:
 *  <ol>
 *   <li> User Table : "user" </li>
 *   <li> Index Table: "personally_enrolled" </li>
 * </ol>
 *  The table's have to be structured in a specific way for this class to work.
 *  The user results will be return in PHP Object format.
 */
class UserRepository extends Repository implements UserModelMethods
    ,BinaryTreeMethods
{
    const FIXED_TREE_METHOD = 'fixedTreeMethod';
    const ADJACENT_METHOD = "adjMethod";
    const NESTED_SET_METHOD = "nestedSetMethod";
                          
    private $userTable = "users";
    private $btMapTable = "personally_enrolled";

    //Default column names
    /** TODO: Check the existence of following columns before construction **/
    private $col_user_pk    = "user_ID";
    private $col_user_name  = "user_Name";
    private $col_user_email = "user_Email";
    private $col_user_btid      = "enrolled_ID"; //Binary tree ID/ node position
    //    private $col_user_parentid= "enroller_id"; //Not necessary yet

    private $col_btmap_pk    = "enroller_ID";
    private $col_btmap_left  = "left_mem";
    private $col_btmap_right = "right_mem";
    private $config;
    

    public function __construct(GenericDatabase $db, $method = null, $config = null)    
    {
        if ($config == null){
            $config = array(
                'method' => UserRepository::FIXED_TREE_METHOD, //'method'   => UserRepository::ADJACENT_METHOD,
                'user_table' => "users",
                'user_pk'    => "user_id",
                'user_name'  => "user_name",
                'user_btid'  => "enroller_id",   //Deprecated
                'user_sponsor_id' => 'enroller_ID',
                'user_enrolled_id' => 'enrolled_ID',
                'user_alias' =>  'alias',
                'user_token'  => 'token',
                'index_table' => "FixedTreeIndex",
                'index_fkey'  => "user_key",
                'index_tree'  => "master_tree",
                'index_position' => "position",
                'contact_table' => 'contact_details',
                'contact_fkey'  => 'user_ID',
                'contact_mobile'=> 'mobile_Number',
                'contact_office'=> 'office_Number',
                'contact_home'  => 'home_Number',
                'contact_title' => 'title',
                'contact_address' => 'address',
                'max_level' => "10"
            );
            if ($method != null)
                $config['method'] = $method;
            $configObj = new \stdClass(); //(object) $this->config;
            foreach($config as $key => $value){
                $configObj->$key = $value;
            }
            $config = $configObj;

        }
        $this->config = $config;
        parent::__construct($db);

    }


    /**
     * Searches for user table's primary key based on the enrolled key.
     *
     * @return String <code>null</code> if not found. Otherwise returns the matching User record's  primary key.
     **/
    public function getUserID($binTreeID){
        $result = parent::retrieve($this->userTable, parent::ALL);  //SELECT ALL FROM USER
        //TODO: Refactor this code to improve fetch speed.
        $found = null;
        foreach($result as $row){
            $btId = $row[$this->col_user_btid];
            if( $btId  == $binTreeID)
                $found = $row[$this->col_user_pk];
        }
        return $found;
    }
    
    /**
     * Gets the tree position id of a given user.
     *
     * Throws an Exception if user is not found.
     * @param String $usrId the User ID key of that user.
     * @param Integer $treeNum The internal tree number of the user to select.
     * @returns null on failure. Otherwise returns the positionID.
     **/
    public function getPositionID($usrID, $treeNum = null){
        //Convert config array into an object
        $config = $this->config;        
        //        $config->method = UserRepository::FIXED_TREE_METHOD;
        
        if (isset($config->method) && $config->method == UserRepository::FIXED_TREE_METHOD){
            //$usr = $this->getUser($usrID);
            /*
            $records = parent::searchByKey(       $config->index_table,
                                                  $config->index_fkey,
                                                  $usrID
                                                  );*/
            if ($treeNum != null)
            $records = parent::selectByKeys( $config->index_table,
                                            array(
                                                "{$config->index_fkey}"
                                                => "{$usrID}",
                                                "{$config->index_tree}"
                                                => "{$treeNum}"
                                            ));
            else
                $records = parent::selectByKeys( $config->index_table,
                                                 array(
                                                     "{$config->index_fkey}"
                                                     => "{$usrID}"
                                                 ));
            
            if($records === null){
                throw new \Exception ("User is not found in any tree!");
                return null;
            }

            if (count($records) ==  1)
                return $records[0][$config->index_position];
            elseif(count($records > 1))
                return $records;
                
            /*$record = null;
            foreach($records as $rec){
                if ($rec[$config->index_tree] == $treeNum)
                    $record = $rec;
            }
            if($record){
                return $record[$config->index_position];
                }*/
            return null;
        }else{
            $usr = $this->getUser($usrID);
            if($usr)
                return $usr->getBTId();
        }
        return null;
    }

    /**
     * Returns the root users of all trees
     * @returns UserModel[] Array of UserModel objects containing the the root user data. Returns null if no master user exists in the database.
     **/
    public function getAllRootUsers(){
        $res = $this->getColumnArray($this->config->index_table,
                                     $this->config->index_tree
        );
        if($res){
            $rootUsers = array();
            foreach($res as $item){
                $rootUsers[$item[$this->config->index_tree]] = $this->getUser($item[$this->config->index_fkey]);                
            }
            return $rootUsers;
        }
        return null;
    }

    /**
     * Gets a root user belonging to a specific tree
     * @param 
     * @return UserModel UserModel object of the root user of that specific tree.
     **/
    public function getRootUser($treeId){
        $users = $this->getAllRootUsers();
        if($users){
            return $users[$treeId];
        }
        return null;
    }

    /**
     * Returns the tree id[s] the given user belongs.
     * As of yet (26/09/16), multiple tree's are not supported.
     * @todo Return an array if the user is found in multiple trees.
     **/
    public function getTreeId(UserModel $user)
    {
        $config = $this->config;
        $userId = $user->getID();
        $record = parent::retrieveByKey($config->index_table,
                                        $config->index_fkey,
                                        $userId);
        return $record[$config->index_tree];
        /*      $pos = $this->getPositionID($userId);
        
        if($pos){
            if (is_array($pos)){ //User is in many trees
                
            }else{ //User is in a single tree
                /*                record = parent::retrieveByKey($this->config->index_table,
                                               $this->config->index_fkey,
                                               );

                $record = parent::retrieveByKey($config->index_table,
                                                $config->index_fkey,
                                                $userId);
                echo var_dump($record);
                return $record[$config->index_tree];
            }
            
        }*/
        //user not in any tree
        //        return null;
    }

    /**
     * Tests whether the given user is a master user
     **/
    public function isMasterUser(UserModel $user){
        $posId = $this->getPositionID( $user->getID() );
        
        if($posId == '1')
            return true;
        return false;
    }

    /**
     * Fetches the user record given the primary key 
     *
     * @returns UserModel UserModel object containing the data. <code>null</code> if primary key is not found.
     */
    public function getUser($usrId){
        $user = null;
        $cfg = $this->config;
        $record = parent::retrieveByKey($this->userTable,
                                        $this->col_user_pk,
                                        $usrId);
        if ($record){
            $user = new UserModel($record[$this->col_user_pk],
                                  $record[$this->col_user_name],
                                  $record[$this->col_user_email],
                                  $record[$this->col_user_btid] );
            // Feature added on Wed 14 Sep 12:30:44 SGT 2016
            //$user->setEnrollerId($record[$this->col_btmap_pk]); //TODO REPLACE
            $user->setEnrollerId($record[$cfg->user_sponsor_id]);
            $user->setAlias($record[$cfg->user_alias]);
            $user->setToken($record[$cfg->user_token]);  //add token to user model
            if($cfg->method == UserRepository::FIXED_TREE_METHOD){
                $treeId = $this->getTreeId($user);
                if($treeId != null){
                    $user->setTreeId($treeId);
                    $pos = $this->getPositionID($usrId, $treeId);  //FIXME: Segmentation fault
                    $user->setTreePos($pos);
                }
            }
            //Fill in contact information if available
            try{
                $crecord = parent::retrieveByKey($cfg->contact_table,
                                      $cfg->contact_fkey,
                                      $usrId);
                $mobileNum  = $crecord[$cfg->contact_mobile];
                $homeNum    = $crecord[$cfg->contact_home];
                $officeNum  = $crecord[$cfg->contact_office];
                $address    = $crecord[$cfg->contact_address];
                //$title      = $crecord[$cfg->contact_title];

                $user->setNumber($mobileNum, $officeNum, $homeNum);
                $user->setAddress($address);
                //$user->setTitle($title);

                //$trecord = 
            }catch(\Exception $ex){
                //ignore errors here
            }
            //get the number of sponsors the user have            
            $countResult = $this->searchByKey($this->userTable,
                               $cfg->user_sponsor_id,
                               $user->getBTId(),
                               "count(user_Name)");
            $user->setSponsorCount($countResult[0]["count(user_Name)"]);
            //ob_start();
            //var_dump($countResult[0])
            //$x = ob_get_clean();
            //$debug_export = var_export($countResult[0]['count(user_Name)'], true);
            //$user->setSponsorCount($debug_export);
            
        }
        if($record == null)
            throw new \Exception(get_class($this) . ": User not found! (PARAM: {$usrId})");
        
        return $user;
    }

    /**
     * Returns a user in the tree using their treeID and Position ID.
     **/
    public function getFixedTreeRecord($treeid, $pos)
    {
        $config = $this->config;
        if($config->method == UserRepository::FIXED_TREE_METHOD)
        {
            $tree_key = $config->index_tree;
            $pos_key  = $config->index_position;
            $user_key = $config->index_fkey;
            $record = parent::retrieveByKeys($config->index_table,
                                             array(
                                                 $tree_key =>
                                                 $treeid,
                                                 $pos_key => $pos
                                             )
            );
            $userID = $record[$user_key];
            $user = $this->getUser($userID);
            return $user;
        }
        return null;
    }

    /*
     * Gets the parent record of the given user.
     * @param UserModel can be a UserModel or an Int
     */
    public function getParentRecord($user)
    {
        $userId = $user;
        $parentPos = null;
        if(!( $user instanceof UserModel)){
            try{
                $user = $this->getUser($userId);
            }catch(\Exception $ex){
                return null;
            }
        }        
        $userId = $user->getID();
        $userTreeId = $user->getTreeId();
        $userPos = $user->getTreePos();
        if($userPos != 1)
            $parentPos = ($userPos - ($userPos % 2))/2;
        else
            $parentPos = $userPos;
        $parent = $this->getFixedTreeRecord($userTreeId, $parentPos);
        return $parent;
    }

    /**
     * Retrieves children's position id for the given record id.
     * @deprecated Not tested for fixed binary tree hierarchy. Avoid iterating over this method this to improve perfomance.
     * @param Integer This method uses the key field instead of UserModel object as a parameter.
     * @return Array Array containing the data null on failure.
     */
    public function getChildren($usrID)
    {
        if ($this->config->method == UserRepository::FIXED_TREE_METHOD){
            $user = $this->getUser($usrID);
            $children = null;
            $pos = $this->getPositionId($usrID);
            $tree_id = $this->getTreeId($user);

            $pos = $pos*2;
            $leftRecord = parent::selectByKeys($this->config->index_table,
                                              array(
                                                  "{$this->config->index_position}"
                                                  => "{$pos}",
                                                  "{$this->config->index_tree}"
                                                  => "{$tree_id}"
                                              )

            );
            $pos = (int)($pos+1);
            $rightRecord = parent::selectByKeys($this->config->index_table,
                                              array(
                                                  "{$this->config->index_position}"
                                                  => "{$pos}",
                                                  "{$this->config->index_tree}"
                                                  => "{$tree_id}"
                                              )

            );
            
            $left = null; $right = null;
            if($leftRecord)
                $left = $this->getUser($leftRecord[0][$this->config->index_fkey]);
            if($rightRecord)
                $right = $this->getUser($rightRecord[0][$this->config->index_fkey]);
            
            return array($left, $right);
        }
        else{
            $children = null;
            $pos = $this->getPositionID($usrID);
            $record = parent::retrieveByKey($this->btMapTable,
                                            $this->col_btmap_pk,
                                            $pos);
            if ($record) {
                $children = array( $record[$this->col_btmap_left],
                                   $record[$this->col_btmap_right] );
            }
            return $children;
        }
    }

    /**
     * Gets user_id to binaryTree position mapping, of an entire tree.
     * @return null when the table is empty. Otherwise returns an array map containing [user_id => TreePosID]
     **/
    public function getUserMap($treeId = 0){
        //TODO:: REFACTOR THIS CODE
        
        $config = $this->config;
        if($config->method == UserRepository::FIXED_TREE_METHOD){
            //FIXED TREE METHOD
            $result = parent::searchByKey($config->index_table,
                                          $config->index_tree,
                                          $treeId);
            if($result == null)
               throw new \Exception ("Cannot get usermap. User may not be in any tree.");
            
            $map = array();
            $key = $config->index_fkey;
            $val = $config->index_position;
            if($result){
                foreach($result as $row){
                    //$map[$row[$key]] = $row[$val]; //Removed: Not intuitive.
                    $map[$row[$val]] = $row[$key];
                }
                return $map;
            }
            return null;
        }
        else
        {  //Adjacency Method: Deprecated
            $key = $this->col_user_pk;
            $val = $this->col_user_btid;
            $map = array();
            $result = parent::retrieve($this->userTable, "{$key}, {$val}");
            //        $result = parent::retrieve("users");
            if($result){
                foreach($result as $row){
                    $map[$row[$key]] = $row[$val];
                }
                return $map;
            }else{
                echo "getUserMap:: results were empty. ";  //DEBUG STATEMENT
            }        
            return null;
        }
    }

    /**
     * Returns all user ids from user table as an array
     **/
    public function getAllUserIds(){
        $key = $this->col_user_pk;
        $results = parent::retrieve($this->userTable, "{$key}");
        $user_ids = null;
        if($results){
            $user_ids = array();
            $i=0;
            foreach($results as $row){
                $user_ids[$i++] = $row[$key]; 
            }
        }
        
        return $user_ids;
        
    }

    /**
     * Returns number of records in the user table.
     **/
    public function countUsers(){
        $result = parent::retrieve($this->userTable);
        return sizeof($result);
    }

    /**
     * Creates a new user and returns their tree id.
     **/
    public function createNewRootUser(UserModel $user){
        //Check whether user exists in a tree
        //if (
        $config = $this->config;
        //Retrieve the next free tree
        $free_tree = $this->getLastTreeIndex()+1;
        $success = $this->addRecord($this->config->index_table, array($config->index_fkey=> $user->getId(),
                                                            $config->index_position => '1',
                                                            $config->index_tree => $free_tree
        ));
        if(!$success)
            throw new \Exception("Cannot add record");
        
    }

    /**
     * Delete root user of a tree
     **/
    public function deleteRootUser(UserModel $user)
    {
        //Check whether user is a root user first
        //        try{
        if (!$this->isMasterUser($user))
            throw new \Exception("User is not a master of any tree.");
        //}catch(\Exception $ex)
        /*{
            throw new \Exception("Root user does not exist in any tree. Please make sure to add the user to the");
            }*/
       
        $cfg = $this->config;
        $success =  $this->deleteRecord($cfg->index_table, $cfg->index_fkey
                                                     , $user->getId()
        );
        if(!$success)
            throw new \Exception("Delete operation failed.");       
    }
    
    /**
     * Gets last active tree
     **/
    public function getLastTreeIndex(){
        $index_table = $this->config->index_table;
        $tree_col    = "MAX({$this->config->index_tree})";
        //$query = "SELECT MAX({$tree_col}) FROM {$index_table};";
        //$results = $this->executeSQL($query);
        $results = $this->retrieve($index_table, $tree_col);
        if($results)
            return $results[0][$tree_col];
        return null;            
    }

    /**
     * Adds a user to a tree
     **/
        public function addUserToTree(UserModel $user, UserModel $sponsor, UserModel $parent, UserModel $master = null, $dir = null){
        //Validate parameters
        $sponsorTree = $this->getTreeId($sponsor);
        if($master == null)
            $master = $this->getRootUser($sponsorTree);
        $parentTree  = $this->getTreeId($parent);
        $masterTree = $this->getTreeId($master);


        if ($sponsorTree != $parentTree && $parentTree != $masterTree)
        {
            throw new \Exception("Sponsor and parent must exist in the same tree");
            return false;
        }
        //Check whether user exists in a tree
        $userTreeId = $this->getTreeId($user);
        if ($userTreeId != null) //TODO Modifythis to support multiple tree for each user
        {
            throw new \Exception("User already exists in a tree");
            return false;
        }
        
        $pos = $parent->getTreePos();
         
        $child = $this->getChildren($parent->getId());
        
        if($dir==null){
            //Get the next free child
            $childPos = 0;
            $freeChild = $child[$childPos];
            if($freeChild != null ){
                $childPos += 1;
            }
            $freeChild = $child[$childPos];
            
            if ($freeChild != null  ){
                throw new \Exception("Parent already full.");
                return false;
            }
            
            
            $freePos = ($pos*2)+$childPos;
        }else{
            if($child[$dir] != null ){
                throw new \Exception("Cannot place a new node in that direction. Direction already taken.");
                return false;
            }
            $freePos = ($pos*2)+$dir;
        }

        $config = $this->config;
        $success = $this->addRecord($this->config->index_table, array
                                    (
                                        $config->index_position => $freePos,
                                        $config->index_tree => $masterTree,
                                        $config->index_fkey => $user->getId()
                                    )
        );
        
        return $success;
    }
    
    
    /**
     * Removes a user from the tree
     **/
    public function removeUser(UserModel $user, UserModel $treeMaster=null){
        $treeId = $this->getTreeId($user);
        return $this->deleteRecordWhere($this->config->index_table, array(
            $this->config->index_fkey => $user->getId(),
            $this->config->index_tree => $treeId
        ));
    }

}

/**
 * Repository provides access to the database.
 * Currently, only read only access is provided.
 * Uses SQL Query to access database.
 **/
class Repository{
    private $db, $limit;
    //    private $tables;
    
    CONST ALL = "*";
    CONST LIMIT = "1024";
    CONST BOOL_AND = "AND";
    CONST BOOL_OR  = "OR";
   
    
    public function __construct(GenericDatabase $db, $limit = self::LIMIT)
    {
        $this->db = $db;
        $tables = array();
        $this->$limit = $limit;
    }

    /**
     * Retrieves a result from the table. 
     *
     **/
    public function retrieve($tableName, $item = self::ALL){
        $limit = self::LIMIT;
        $retrieveQuery = "SELECT {$item} FROM {$tableName} LIMIT :limit;";
        $this->db->query($retrieveQuery);
        $this->db->bind(":limit", (int) $limit);
        $results = $this->db->results();
        return $results;
    }


    /**
     * Retrieve a single record using a key field.
     *
     */
    public function retrieveByKey($tableName, $key, $val, $item = self::ALL){
        //$query = "SELECT {$item} FROM {$tableName} WHERE $key = ':val' ;";
        $query = "SELECT {$item} FROM {$tableName} WHERE $key = '{$val}' ;";
        //echo $query ."\n </br>";
        $this->db->query($query);
        //$this->db->bind(":val", (string) $val);
        $results = $this->db->results();
        //        echo ":val = ".$val . " <br /> \n";
        //echo $query. " - ". var_dump($results[0] != null). "<br /> \n";
        if($results)
            return $results[0];
        return null;
    }

    /**
     * Delete record from table
     **/
    public function deleteRecord($tableName, $key, $value){
        $query = "DELETE FROM {$tableName} WHERE $key = $value;";
        //echo "\n {$query}\n";
        $this->db->query($query);
        $result = $this->db->results();
        if ($this->db->success()){
            return true;
        }
        return false;
    }

    /**
     * Multi Conditional delete. Pass associative array as the condition list
     **/
    public function deleteRecordWhere($tableName, $conditions){
        if (array_keys($conditions) === range(0, count($conditions) -1))
            throw new \Exception(get_class($this). ": Argument \$items must be an associative array.");
        
        $condStr = "";
        $i=0;
        $len=count($conditions);
        
        foreach($conditions as $key => $value){
            //$condStr .= " {$key} = :val{$i}";
            $condStr .= " `{$key}` = '{$value}'";
          if($i != $len - 1)
              $condStr .= " AND";
          $i++;
        }

        
        $query = "DELETE FROM {$tableName} WHERE {$condStr};";

        /*$i = 0;
        foreach($conditions as $key => $value){            
            $this->db->bind(":val{$i}", (string) $value);
            echo "{$key} => {$value}\n";
            $i++;
            }*/
        
        //echo $query;
        $this->db->query($query);
        $results = $this->db->results();
        
        return $this->db->success();
    }

    /**
     * Update value from the table
     **/
    public function updateRecords($tableName, array $conds, array $items){
        //Check validity of $items parameter
        if (array_keys($items) === range(0, count($items) -1))
            throw new \Exception(get_class($this). ": Argument \$items must be an associative array.");
        
        if (array_keys($conds) === range(0, count($conds) -1))
            throw new \Exception(get_class($this). ": Argument \$conds must be an associative array.");
        
        $rowStr = "";
        $i=0;
        $len=count($items);
        $valueStr = "";
        $condStr = "";

        foreach($conds as $key => $item){
            $condStr .= "`". $key. "`";
            $condStr .= "= ";
            $condStr .= "{$item}";
            
        }

        foreach ($items as $key => $item){
            $valueStr .= "". $key . "";
            $valueStr .= "= '{$item}'";           
            if($i != $len - 1){
              $rowStr .= ", ";
              $valueStr .= ", ";
            }
            $i++;
        }

        $query = "UPDATE {$tableName} SET {$valueStr} WHERE {$condStr};";
        $this->db->query($query);
        
        $results = $this->db->results();
        return $this->db->success();
    }
    

    
    /**
     * Insert values into table
     **/
    public function addRecord($tableName, array $items){
        //Check validity of $items parameter
        if (array_keys($items) === range(0, count($items) -1))
            throw new \Exception(get_class($this). ": Argument \$items must be an associative array.");
        $rowStr = "";
        $i=0;
        $len=count($items);
        $valueStr = "";
        $condStr = "";

        foreach ($items as $key => $item){
            $rowStr .= "`". $key . "`";
            $valueStr .= "'{$item}'";
            if($i != $len - 1){
              $rowStr .= ", ";
              $valueStr .= ", ";
            }
            $i++;
        }

        $query = "INSERT INTO {$tableName} ({$rowStr}) VALUES ({$valueStr})";        
        //echo var_dump($query);
        $this->db->query($query);
        $results = $this->db->results();

        return $this->db->success();
        /* if ($this->db->success() == ){
            return true;
        }
                
        return false;*/
    }
    
    /**
     * Retrieve a multiple records using a key field with multiple conditions.
     *
     */
    public function selectByKeys($tableName, $cond, $bool = self::BOOL_AND, $item = self::ALL){
        $condStr = "";
        $i=0;
        $len=count($cond);
        foreach($cond as $key => $value){
          $condStr .= " {$key} = :val{$i}";
          if($i != $len - 1)
              $condStr .= " AND";
          $i++;
        }
        
        $query = "SELECT {$item} FROM {$tableName} WHERE {$condStr};";
        $this->db->query($query);

        $i = 0;
        foreach($cond as $key => $value){            
            $this->db->bind(":val{$i}", (string) $value);
            $i++;
        }
        
        $results = $this->db->results();
        return $results;
    }

    /**
     * Retrieve a single record using a keyfield with multiple conditions
     */
    public function retrieveByKeys($tableName, $cond, $bool = self::BOOL_AND, $item = self::ALL){
        $results  = $this->selectByKeys($tableName,
                                        $cond,
                                        $bool,
                                        $item);
        if ($results)
            return $results[0];
        return null;
    }
   
    /**
     * Retrieve multiple record using a key field.
     *
     */
    public function searchByKey($tableName, $key, $val, $item = self::ALL){
        $query = "SELECT {$item} FROM {$tableName} WHERE $key = :val ;";
        //echo $query . " :val = {$val} ; $\n<br />";
        $this->db->query($query);
        $this->db->bind(":val", (string) $val);
        $results = $this->db->results();        
        return $results;
    }

    /**
     * Get all the unique columns in a table as an array
     **/
    public function getColumnArray($tableName, $colName)
    {
        $query = "SELECT * FROM {$tableName} GROUP BY {$colName}";
        $this->db->query($query);
        $result = $this->db->results();
        return $result;
    }

    /** Validates the existence of all tables
     * @returns true on success, false on error.
     **/
    //public function validateConfig(){
    //    return false;
    //}
}

/**
 * Abstract class for Database Builder pattern.
 **/
abstract class AbstractDatabaseConfigBuilder{
    abstract public function getDatabaseConfig();
}

/**
 * Abstract class for Database Builder Pattern
 **/
abstract class AbstractDatabaseConfigDirector{
    abstract public function getDatabaseConfig();
    abstract public function build();
}

/**
 * Database configuration class
 * 
 **/
class DatabaseConfig{
    private $user, $pass, $hostName, $type, $db;
    public function __construct(){}
    //--- Setters  ---/.
    public function setHost($hostName){ $this->hostName = $hostName; }
    public function setType($type){ $this->type = $type;  }
    public function setDB($db){ $this->db = $db; }
    public function setUser($user){ $this->user = $user; }
    public function setPass($pass) { $this->pass = $pass; }
    //-- Getters  --//
    public function getHost(){ return $this->hostName; }
    public function getUser(){ return $this->user; }
    public function getPass(){ return $this->pass; }
    public function getType(){ return $this->type; }
    public function getDB(){   return $this->db; }
}

/**
 * <Builder for DatabaseConfig Builder pattern>
 */
class DatabaseConfigBuilder extends AbstractDatabaseConfigBuilder{
    private $dbConfig  = null;

    public function __construct(){
        $this->dbConfig = new DatabaseConfig();
    }
    /** Sets the database server host **/
    public function setHost($host){ $this->dbConfig->setHost($host); return $this;}
    /** Set the user name **/
    public function setUser($user){ $this->dbConfig->setUser($user); return $this;}
    /** Set the user password **/
    public function setPass($pass){ $this->dbConfig->setPass($pass); return $this;}
    /** Set the database name **/
    public function setDB($db) {    $this->dbConfig->setDB($db); return $this;}
    /** Set the type of this server, if defined. **/
    public function setType($type) {  $this->dbConfig->setType($type); return $this;}
    /** Returns the database configuration **/
    public function getDatabaseConfig(){ return $this->dbConfig; return $this;}    
}

/**
 * Build using the default configurations.
 * This configuration builder should be used for test purposes.
 * Assumes the following configuration.
 * <code>
 *  host = 'localhost'
 *  user = 'root'
 *  pass = '123'
 * </code>
 */
class TestDatabaseConfig extends  AbstractDatabaseConfigDirector{
    private $builder = null;

    public function __construct(AbstractDatabaseConfigBuilder $builder){
        $this->builder = $builder;
    }
    
    public function getDatabaseConfig(){ return $this->builder->getDatabaseConfig(); }
    public function build(){
        $this->builder->setHost("localhost");
        $this->builder->setUser("root");
        $this->builder->setPass("123");
    }
}

/**
 * Build database configuration using the php configurations variable.
 */
class DatabaseConfigVar extends  AbstractDatabaseConfigDirector{
    private $builder = null;
    private $fail   = false;
    private $cfg  =  null;
    /**
     * Default constructor.
     * Associative array passed into the constructor must define the following keys -> {'host', 'pass', 'name', 'user'}.
     * @param AbstractDatabaseConfigBuilder $builder The preferred configuration builder
     * @param $assoc Associative Array containing all the necessary configurations defined.
     **/
    public function __construct(AbstractDatabaseConfigBuilder $builder, $assoc){
        $this->builder = $builder;

        $test_keys = array('host', 'pass', 'name', 'user');
        $this->cfg = $assoc;

        foreach($test_keys as $key){
            if(! isset($assoc[$key]) )
                $this->fail = true;
        }
                  
    }

    /**
     * Returns the database configuration
     **/
    public function getDatabaseConfig(){
        return $this->builder->getDatabaseConfig();
    }

    /**
     * Builds the configuration.
     **/
    public function build(){
        $cfg = $this->cfg;
        if($this->fail)   
            return false;   //Bad argument given
        $this->builder->setHost($cfg['host']);
        $this->builder->setUser($cfg["user"]);
        $this->builder->setPass($cfg['pass']);
        $this->builder->setDB($cfg['name']);                
        return true;
    }

    /**
     * Quickly creates a new instance of this class and builds the configuration.
     * Use this if you do not intend to modify the user configurations.
     **/
    public static function quickCfg($cfg){
        $builder = new DatabaseConfigBuilder();
        $instance = new self($builder, $cfg);
        $instance->build();
        return $instance->getDatabaseConfig();
    }
}


/**
//Deprecated since 1.0
abstract class Abstract_BinaryTree_Table{
 const DEFAULT_KEYFIELD = 'enroller_id';
 const DEFAULT_LFIELD   = 'left_mem';
 const DEFAULT_RFIELD   = 'right_mem';
 }**/

/** abstract class AbstractDatabaseFactory{
     const DB_MYSQL = 100;
     const DB_ORACLEDB   = 101;
     abstract function createNewDatabase();
     }**/

/**
 * Layers PDO database methods as a Generic Database.
 **/
class PDO_Database implements GenericDatabase{
    private $m_db;
    private $m_statement, $m_result;
    private $m_qCount = 0;

    /**
     *  Construct the database using PDO Database Object
     */
    public function __construct(PDO $pdo)
    {
        $this->m_db = $pdo;
        $this->m_result = false;
    }

    public function query($query)
    {
        $this->m_statement = $this->m_db->prepare($query);        
    }

    public function results(){
        $this->m_result = $this->execute();
        return $this->m_statement->fetchAll(PDO::FETCH_ASSOC);
    }

    public function bind($pos, $value, $type=null){
        if(is_null($type)){
             switch (true) {
                case is_int($value):
                    $type = PDO::PARAM_INT;
                    break;
                case is_bool($value):
                    $type = PDO::PARAM_BOOL;
                    break;
                case is_null($value):
                    $type = PDO::PARAM_NULL;
                    break;
                default:
                    $type = PDO::PARAM_STR;
            }
        }
        $this->m_statement->bindValue($pos, $value, $type);
    }

    public function success(){
        return $this->m_result;
    }

    private function execute(){
        $this->m_qCount++;
        return $this->m_statement->execute();
    }
}


/**
 * PDO creator with user configuration. 
 **/
class PDO_DefaultDatabase extends PDO_Database{
    public function __construct($user, $pass, $dbname, $host='localhost'){

        //        $host="localhost";
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_PERSISTENT => true
        );
        
        try{
            $pdo = new PDO("mysql:host=".$host.";dbname=".$dbname, $user, $pass, $options);
            parent::__construct($pdo);
        }catch(PDOException $ex){
            //echo $ex->getMessage();
            exit();
        }
    }    
}

class PDO_UserDatabase extends PDO_Database{
    public function __construct(DatabaseConfig $config =null ){
        if(!$config){
            //default config
            $config = new DatabaseConfig();
            //            $config->setHost('localhost');
            $config->setHost('192.168.0.13');
            $config->setPass('123');
            $config->setDB('mlm_db2');
            //$config->setUser('root');
            $config->setUser('shath');
        }
           
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_PERSISTENT => true
        );

        $user = $config->getUser();
        $pass = $config->getPass();
        $dbname=$config->getDB();
        $host = $config->getHost();
        
        try{
            $pdo = new PDO("mysql:host=".$host.";dbname=".$dbname, $user, $pass, $options);
            parent::__construct($pdo);
        }catch(PDOException $ex){
            echo $ex->getMessage();
            exit();
        }

    }
}
        

?>