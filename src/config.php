<?php
/**
 * Stores the default database configuration.
 * Loads ../config.php if it exists.
 * 
 * Config php file should be defined as follows:
 * <code>
 *   namespace BinaryTreeVisualizer;
 *   Config::  $db_config = array(
 *   'host' => "localws",
 *   'pass' => "123",
 *   'name' => 'mlm_database',
 *   'user' => 'root'
 *    );
 * </code>
 **/
namespace BinaryTreeVisualizer;

/**
 * This class contains the default database configurations.
 **/
class Config{
    public static $db_config = array(
        'host' => "192.168.0.16",
        'pass' => "123",
        'name' => 'mlm_database',
        'user' => 'shath'
    );    
}

$path = dirname(__FILE__).'/';
if ( file_exists($path.'../config.php') ){
    require_once ($path.'../config.php');
}
if ( file_exists($path.'../../config.php') ) 
    require_once ($path.'../../config.php');

//$GLOBAL['db_config'] = $db_config;
?>