<?php 
namespace BinaryTreeVisualizer\Graphics;

/*
  //to be implemented in the future
  //View helper 
  interface HelperView{
   public function parseNode();
   public function getHTML();
  }
 */

/**
 * Interface for Javascript Graphics Object. 
 * These objects are used by the front-end coder for visualization purposes.
 **/
interface JSGraphicsScript{
    /** If the display node is an object, index of the string. **/
    public function setObjectLabel($param);
    public function getHeader();
    public function getScript();
    public function setContainer($domID);
    //public function setViewHelper(HelperView $helperClassName);    // TODO, if view helper is set, the view helper html object will be displayed instead of the text label.
    //public function setJavaScript();
}


/**
 * Display label object
 * @deprecated 1.0 In favour of Models.
 **/
abstract class DisplayLabel{
    const OBJECT="object";
    const STRING=null;
}

?>