<?php
/**
 *  Graph classes.
 *
 * @author shath ibrahim <shath.ibrahim@gmail.com> 
 * @version 1.0
 **/

namespace BinaryTreeVisualizer;
//TODO: Refactor and clean the code.
include_once 'BinaryTree.class.php';

/**
 * A generic node used by this module.
 **/
class GraphNode extends GenericNode{
    /**
     * Construct a node using a static method.
     * @param $data Link data with this node.
     **/
    public static function withData($data){
        $instance = new self();
        $instance->setData($data);
        return $instance;
    }
}

/**
 * Class for Graph Edge object. No longer used in the updated version.
 * @todo Remove this and refactor the code.
 * @deprecated 1.0
 **/
class GraphEdge{
    private $nodeA, $nodeB;
    public function __construct($nodeA, $nodeB){
        $this->$nodeA=$nodeA;
        $this->$nodeB=$nodeB;
    }
    public function getNodes(){
        return array($nodeA, $nodeB);
    }
}

/**
 * A basic Graph Data structure.
 * 
 * Reference: https://en.wikipedia.org/wiki/Graph_%28abstract_data_type%29
 **/
class Graph{
    private $nodes, $edges, $size;
    protected $digraph = false;   //TODO: Modify methods to support digraphs
    public function getNodes(){ return $this->nodes;}
    public function getEdges(){ return $this->edges; }

    /**
     * Default constructor. Prepares the graph object.
     * @param Set true for a directed graph. [Default=false]
     **/
    public function __construct($digraphMode = false){
        $this->nodes = array();
        $this->edges = array();
        $this->digraph = $digraphMode;
    }

    /**
     * Adds node to the graph.
     * @param GraphNode $node Node to add.
     **/
    public function addNode(GraphNode $node){
        $this->size++;
        return array_push($this->nodes, $node)-1;
    }

    /**
     * Connects two nodes.
     * Both nodes must exist in the graph system. So, make sure to add the nodes 
     * before connecting them.
     *
     * @param GraphNode $nodeA firstNode to connect
     * @param GraphNode $nodeB secondNode to connect.
     * @return true on sucess.
     **/
    public function addEdge(GraphNode $nodeA, GraphNode $nodeB)
    {
        $keyA = array_search($nodeA, $this->nodes);
        $keyB = array_search($nodeB, $this->nodes);
        //if(!$keyA !== false || !$keyB !== false)
        //    return false;
        $cond1 = in_array( array($keyA, $keyB), $this->edges);
        if(!$cond1 ){
            array_push($this->edges, array($keyA, $keyB) );
        }

        if (!$this->digraph){
            $cond2 = in_array( array($keyB, $keyA), $this->edges);
            if(!$cond2)
                array_push($this->edges, array($keyB, $keyA) );            
        }
                
        return true;
    }

    /**
     * Removes connection between two nodes.
     * Both nodes and a connection between them must exists in the system.
     *
     * @param GraphNode $nodeA fromNode, where connection starts, if a directional graph.
     * @param GraphNode $nodeB toNode, where connection ends, if a directional graph.
     **/
    public function removeEdge(GraphNode $nodeA, GraphNode $nodeB){

        $keyA = array_search($nodeA, $this->nodes);       
        $keyB = array_search($nodeB, $this->nodes);

        $keyRemoveA = array_search(array($keyA, $keyB), $this->edges);
        unset($this->edges[$keyRemoveA]);

        if(!$this->digraph){
            $keyRemoveB = array_search(array($keyB, $keyA), $this->edges);
            unset($this->edges[$keyRemoveB]);            
        }
        
    }

    /**
     * Removes a node from the Graph.
     * The node must exist in the graph.
     *
     * @return On sucess, returns true. Otherwise returns false.
     **/
    public function removeNode(GraphNode $node)
    {
        $nodeKey = array_search($node, $this->nodes);

        if ($nodeKey !== false){
            //unset($this->nodes[$nodeKey]);
            array_splice($this->nodes, $nodeKey, 1);
            $this->size--;
            
            $edgeList = $this->edges;
            for($i=0; $i< sizeof($edgeList); $i++ ){
                $edge = $edgeList[$i];
                if($edge[0] == $nodeKey || $edge[1] == $nodeKey)
                    unset($this->edges[$i]);                
            }
            $this->size--;
            return true;
        }
        return false;
    }

    /**
     * Check whether the given node exists in the graph
     * @todo Give this method a more appropriate name.
     * @param GraphNode $node Node to test
     * @returns false upon failure. 
     **/
    public function getNode(GraphNode $node){
        $nodeKey = array_search($node, $this->nodes);
        if($nodeKey !== false)
        {
            //return $nodeKey;
            return $this->nodes[$nodeKey];
        }
        return false;
    }

    /**
     * Retrieves the node with the node id.
     * @return GraphNode node
     **/
    public function getNodeWithId($nodeId){
        return $this->nodes[$nodeId];
    }

    /**
     * Searches the graph for a given node and returns it's index if found.
     * @return int index of the node in private storage if found
     **/
    public function getNodeId(GraphNode $node){
        if($key = array_search($node, $this->nodes)){
            return $key;
        }
        return "0";
    }

    /**
     * Checks whether the given two nodes are neighbours.
     * @param GraphNode $nodeA
     * @param GraphNode $nodeB
     **/
    public function isNeighbour(GraphNode $nodeA, GraphNode $nodeB)
    {
        $keyA = array_search($nodeA, $this->nodes);
        $keyB = array_search($nodeB, $this->nodes);
        $key = array_search( array($keyA, $keyB), $this->edges);
        if( $key !== false)
        {
            return true;
        }
        return false;
    }

    /**
     * Count the size of the graph.
     * @return integer number of nodes in the graph.
     **/
    public function count(){
        return $this->size;
        //return sizeof($this->nodes);
    }

}

?>