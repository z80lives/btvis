<?php
namespace BinaryTreeVisualizer\TreeGenerator;
// TODO: Refactor the tree traversal code.

include_once 'DatabaseWrapper.php';
include_once 'BinaryTree.class.php';
include_once 'Graph.class.php';

//TODO: Make the database configurable
use BinaryTreeVisualizer\DB_Connector\GenericDatabase as Database;
use BinaryTreeVisualizer\DB_Connector\UserRepository as Repository;

use BinaryTreeVisualizer\BinaryTree;
use BinaryTreeVisualizer\BinaryTreeNode;
use BinaryTreeVisualizer\BinaryTreeCallable;
use BinaryTreeVisualizer\Graph;
use BinaryTreeVisualizer\GraphNode;

use BinaryTreeVisualizer\DB_Connector\Models\UserModel;


/**
 * Default configuration for how tree nodes should be stored.
 **/
class TreeConfig{
    public $rootUsrId = "1";//= null;  // MUST BE SET FOR FIXED TREE
    public $rootPosId = "SBC1";
    public $fixedTreeMode = false;
    public $fixedPosRoot = 1;
    public $treeNumber = 0;
    public $prefix = "SBC";
    public $fillEmptyNodes   = true;
    public $balanceLevel   = 1;  //Add to level of the lastNode
    public function getRoot(){return $this->rootPosId;}
}

class TreeConfigMaker extends TreeConfig{
    public function setRoot($posId){ $this->rootPosId = $posId; }
}


interface TreeGenerator{
    public function makeTree($cfg);
}

/**
 * The default tree generator. 
 *
 * TODO: This class needs heavy refactoring.
 **/
class DefaultTreeGenerator implements TreeGenerator{
    private $db;
    private $rep;
    private $config;

    private $usrIds;
    private $height=0;
    //private $leaveParent = new Array();
    
    public function __construct(Database $db){
        $this->db  = $db;
        $this->rep = new Repository($db, Repository::ADJACENT_METHOD);
    }

    /**
     * Generates a binary tree from the database
     *
     * @returns null on failure.
     */
    public function makeTree( $config = null){
        $tree = null;
        $userIds = $this->readUsers();  //Contains user ids
        $dbmap = $this->readMappings();
        
        if ($config == null)
            $config = new TreeConfig();
        
        if (gettype($config) == "string"){
            $var = $config;
            $config = new TreeConfig();
            $config->rootPos = $var;
        }
        
        $this->config = $config;


        if($this->validateMappings())
        {
            $graph = new Graph();

            //Create a graph using the database
            //$nodeMap = array();
            foreach($userIds as  $userId)
            {
                
                //if(isset($dbmap[$userId]) ) continue;
                
                //ignore if a user is not mapped on datase map table
                //if (strlen($dbmap[$userId]) == 0)   continue;
                //    $graph->removeNode($tmpNode);

                $user = $this->rep->getUser($userId);
                //$nodeid = $graph->addNode(GraphNode::withData($user->getBTId()));
                if($user){
                  $nodeid = $graph->addNode($tmpNode=GraphNode::withData($user));
                }
                //                if (strlen($dbmap[$userId]) == 0)
                //    $graph->removeNode($tmpNode);
                   
                
                    
                //$nodeMap[$user->getBTId()] = $this->rep->getChildren($userId);
            }

            $nodes = $graph->getNodes();
            // Add link to the graph according to the mappings
            $root = $this->findChild($graph, $config->getRoot());
            if ($root == null)
                throw new \Exception(get_class($this). ": Node to generate the tree is not set.");
            $this->linkChildren($graph, $root);

            
            $tree = $this->convertToTree($graph, $root);
            //Create tree from graph
        }
        //$tree->setRoot($root);
             
        return $tree;
    }

    private function convertToTree(Graph $graph, GraphNode $rootNode)
    {
        $root = BinaryTreeNode::withData($rootNode->getData());
        $this->addNodeToTree($root, $graph , $rootNode);
        
        if($this->config->fillEmptyNodes){
            $this->addEmptyNodes($root);
        }
        
        return BinaryTree::withRoot($root);
    }

    private function addNodeToTree(BinaryTreeNode $btnode, Graph $graph, GraphNode $node){
        $userModel = $node->getData();
        if($userModel){
            $childPos = $this->rep->getChildren( $userModel->getID() );

            if ( sizeof($childPos) == 0)
                return true;
            
            $leftNode = $this->findChild($graph, $childPos[0]);
            $rightNode = $this->findChild($graph, $childPos[1]);

            
            if($leftNode){
                
                $btnode->addChild( BinaryTreeNode::withData($leftNode->getData()),
                               BinaryTreeNode::LEFT);
                $lNode = $btnode->getChildren()[BinaryTreeNode::LEFT];
                $this->addNodeToTree($lNode, $graph, $leftNode);
            }

            if($rightNode){
                
                $btnode->addChild( BinaryTreeNode::withData($rightNode->getData()),
                               BinaryTreeNode::RIGHT);
                $rNode = $btnode->getChildren()[BinaryTreeNode::RIGHT];
                $this->addNodeToTree($rNode, $graph, $rightNode);
            }
            return true;
            
        }
    }

    private function addEmptyNodes(BinaryTreeNode $btNode, $lvl=0)
    {
        $child = $btNode->getChildren();
        if($child != null){
            for($i=0; $i<2; $i++){
                if($child[$i]){ 
                $this->addEmptyNodes($child[$i], $lvl++);
               }
            }
        }

        for($i=0; $i<2; $i++){
            if( $child[$i]==null  ){
                /* $btNode->addChild(BinaryTreeNode::withData(new UserModel("NULL",
                                                                         "Available",
                                                                         "",
                                                                         "NULL"
                )

                ), $i);*/
                //                echo var_dump($btNode->getData()->getBTId());
                $data =  $btNode->getData();
                $nullUser = new UserModel( $data->getID(),
                                           "null",
                                           "as",
                                           $data->getBTId() + "1"+$i
                ); //Available node -> {parentId, null, as, '[parentPos]1[childPos]'}
                                
                $btNode->addChild(BinaryTreeNode::withData($nullUser), $i);
            }
        }
        
        return true;
    }

    
    /**
     * Link nodes according to the database map.
     */ 
    private function linkChildren(Graph $graph, GraphNode $node, $lvl=0){
        if(!$node){
            return false;
        }
        
        //update height of the tree
        if ($this->height < $lvl)
            $this->height = $lvl;
        
        $userModel = $node->getData();

        if($userModel){  // if user is already defined
            $leftNode=$rightNode = null;
            $childrenPos = $this->rep->getChildren( $userModel->getID() );
            
            if ( sizeof($childrenPos) == 0)
                return true;
            if($childrenPos[0] )
                $leftNode = $this->findChild($graph, $childrenPos[0]);
            if($childrenPos[1])
                $rightNode = $this->findChild($graph, $childrenPos[1]);

            
            if($leftNode){
                $this->linkChildren($graph, $leftNode, $lvl++);
                $graph->addEdge($node, $leftNode);
            }
           
           
            
            if($rightNode){
                $this->linkChildren($graph, $rightNode, $lvl++);
                $graph->addEdge($node, $rightNode);
            }
           
           

            
            return true;
        }
        //        $pos = $userModel->getBTId();
    }
    
    /**
     * Return the node with the data id from graph
     *
     * @return user node with the position id from the graph.
     **/
    private function findChild(Graph $graph, $posId){
        $nodes = $graph->getNodes();
        $key = null;
        if(strlen($posId) <= 0) return false;

        for($i=0; $i<  sizeof($nodes); $i++)
        {
            if ($nodes[$i] == null) continue;
            
           
            if($nodes[$i]->getData()->getBTId() == $posId)
            {
                $key = $i;
                //echo var_dump($nodes[$i]->getData()->getName()), $key;
                break;
                
            }
        }

        //        echo var_dump($key);
        if($key !== null )
        {
            return $nodes[$key];
        }

        return false;
    }
    
    private function readUsers(){
        $this->usrIds = $this->rep->getAllUserIds();
        return $this->usrIds;
    }

    private function readMappings(){
        return $this->rep->getUserMap();
        //        return $this->usrMap;
    }

    /**
     * TODO
     * Validates whether the data can be used to create a valid binary tree.
     **/
    private function validateMappings(){
        return true;
    }

}

?>