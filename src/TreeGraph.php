<?php
namespace BinaryTreeVisualizer;

include_once "BinaryTree.class.php";
include_once "Graph.class.php";


use BinaryTreeVisualizer\BinaryTree as BinaryTree;
use BinaryTreeVisualizer\BinaryTreeNode as BinaryTreeNode;
use BinaryTreeVisualizer\BackTracker;

/**
 * Construct a graph from a binary tree
 *
 **/
class TreeGraph extends Graph{
    private $root;
    /**
     * Default constructor
     * @param BinaryTree $tree A valid BinaryTree to convert.
     **/
    public function __construct(BinaryTree $tree)
    {
        parent::__construct();
        $this->root = $tree->getRoot();
        $this->digraph = true;
    }

    /**
     * A static function which builds the tree upon construction.
     *
     * Short hand for :
     * <code>
     * $tg = new TreeGraph($bt);
     * $tg->build();
     * </code>
     * @param BinaryTree $tree A valid BinaryTree to convert.
     * @return TreeGraph Newly created TreeGraph instance.
     **/
    public static function create(BinaryTree $tree){
        $instance = new self($tree);
        $instance->build();
        return $instance;
    }

    /**
     * Starts the conversion process.
     * Fails if the tree is not valid.
     **/
    public function build(){
        $root = $this->root;        
        if(!$root)
            return;        
        $this->addTreeNode($root);
    }

    /**
     * Traverse through the binary tree
     * (Non-recursive and Breadth first),
     * and add every node to the graph.
     * source: https://en.wikipedia.org/wiki/Breadth-first_search
     **/
    /*private function addTreeNode(BinaryTreeNode $node)
    {
        $data = $node->getData();
        $bt  = new BackTracker();
        $nStack = [];
        
        $bt->setMethod("enter",function($n, $p, $lvl, $i){
           $graphNode = GraphNode::withData($n->getData());
           $this->addNode($graphNode);
        });

        $bt->setMethod("exit", function($n, $p, $lvl){
            if($n && $p){
                $nodeA = $this->getNode(GraphNode::withData($n->getData()));
                $nodeB = $this->getNode(GraphNode::withData($p->getData()));
                $this->addEdge($nodeB, $nodeA);
            }
        });
        
        $bt->executeWalk($node);
        }*/
       
    private function addTreeNode(BinaryTreeNode $node){        
        //$data =  $node->getData();
        //$graphNode = GraphNode::withData($data);
        //$this->addNode($graphNode);
        $stack = array();
        $pstack = array();
        
        //$children = $node->getChildren();
        //foreach($children as $child)
        //    array_push($stack, $child);

        array_push($stack, $node);
        while(!empty($stack)){
            //            echo "\n PSTACK=" .var_dump($pstack)."\n ";
            $currentNode = array_shift($stack);
            
            $data=null;
            if($currentNode)
                $data = $currentNode->getData();
            
            //echo var_dump($data);
            $graphNode = null;
            if($data){
                $graphNode = GraphNode::withData($data);
                $this->addNode($graphNode);
                //echo $graphNode->getData();
            }
                        
            //add grand children
            if($currentNode)
                $gchild = $currentNode->getChildren();

            //Connect edge if parent stack contains an element
            if(!empty($pstack)){
                //$child  = array_pop($pstack);
                //$parent = array_pop($pstack);
                //echo $parent->getData()." ->".$child->getData()."\n";
                //echo var_dump($pstack);
                //$this->addEdge($parent, $graphNode);
            }
            
            foreach($gchild as $gc){
                if($gc){
                    //echo $currentNode->getData()." ->".$gc->getData()."\n";
                    //echo $graphNode->getData()." ->".$gc->getData()."\n";
                    array_push($stack, $gc);
                    array_push($pstack, $graphNode);
                    array_push($pstack, $gc);
                    //array_push($pstack, $gc);
                }
            }

            
            //foreach($gchild as $child)
            //  array_push($stack, $child);
        }
        $nodes = $this->getNodes();
        while(!empty($pstack)){
            $parent = array_shift($pstack);
            $child = array_shift($pstack);
            $p = $this->getNodeId(GraphNode::withData($parent->getData()));
            $c = $this->getNodeId(GraphNode::withData($child->getData()));
            $this->addEdge($nodes[$p], $nodes[$c]);
            //echo var_dump($pstack);
        }

    }

    /*    private function addTreeNode(BinaryTreeNode $node){
        //Depth first version
        $data = $node->getData();
        $graphNode = GraphNode::withData($data);
        $this->addNode($graphNode);
        
        $child = $node->getChildren();
        
        if($child[0] || $child[1]){
            for($i=0; $i<2; $i++)
            {
                if(!isset($child[$i]))
                    continue;
                $childGraphNode = $this->addTreeNode($child[$i]);
                $this->addEdge($graphNode, $childGraphNode);  //Connect with the child node
            }
        }
        return $graphNode;
        }
    */
}

?>