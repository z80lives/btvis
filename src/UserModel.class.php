<?php
namespace BinaryTreeVisualizer\DB_Connector\Models;

class Model{
    public function toArray(){
        return (array) $this;
    }
}

class UserModel extends Model{
    CONST LEFT="left";
    CONST RIGHT="right";
    CONST HOME="HOME";
    CONST WORK="OFFICE";
    CONST MOBILE="MOBILE";
    private $pkey, $name, $email, $position, $enroller_pos;
    private $treePos, $treeId;
    private $office_num, $home_num, $mobile_num, $address, $title;
    private $token;
    private $alias;
    private $nSponsor = 0;
    public $extra; //use this variable to store extra information about the record
    public function __construct($pkey, $name, $email, $position){
        $this->pkey = $pkey;
        $this->name = $name;
        $this->email = $email;
        $this->position = $position;
    }
    
    public function getID() { return $this->pkey; }
    public function getName(){ return $this->name;  }
    public function getEmail() { return $this->email; }
    public function getBTId() { return $this->position; }
    public function getEnrollerID(){ return $this->enroller_pos; }
    public function setEnrollerID($enroller_pos){ $this->enroller_pos = $enroller_pos; }
    public function setTreePos($treePos){$this->treePos = $treePos; }
    public function getTreePos(){return $this->treePos;}
    public function getTreeId(){return $this->treeId;}
    public function setTreeId($treeId){$this->treeId = $treeId;}
    public function setAlias($alias) {$this->alias = $alias; }
    public function getAlias() {return $this->alias; }
    public function setSponsorCount($nSponsor){ $this->nSponsor = $nSponsor; }
    public function getSponsorCount(){ return $this->nSponsor; }
    public function setNumber($mobile=null, $work=null, $home=null){
        $this->mobile_num = $mobile;
        $this->office_num = $work;
        $this->home_num   = $home;
    }

    public function getNumber($def=self::MOBILE){
        if($def == self::MOBILE)
            return $this->mobile_num;
        if($def == self::WORK)
            return $this->office_num;
        if($def == self::HOME)
            return $this->home_num;
        return null;
    }

    public function setTitle($title){
        $this->title = $title;
    }
    public function getTitle(){
        return $this->title;
    }

    public function getAddress(){
        return $this->address;
    }

    public function setAddress($address){
        $this->address = $address;
    }

    public function setToken($token){
        $this->token = $token;
    }
    public function getToken(){
        return $this->token;
    }
}

?>