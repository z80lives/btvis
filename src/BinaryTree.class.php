<?php
/**
 * Binary Tree Package
 *
 * 
 * Defines the structure of the binary tree and it's node.
 * GenericNode class is also defined in here. 
 *
 *
 * @todo Refactor GenericNode out of this class
 * @version 1.0
 *
 **/
namespace BinaryTreeVisualizer;

interface Node{
    public function getData();
    public function setData($data);
}

/**
 * Generic node is used by both Binary Tree and Graphs.
 *
 **/
class GenericNode implements Node{
    private $data;
    /**
     * Gets data stored in the node 
     **/
    public function getData() { return $this->data; }
    /**
     * Set the data in the node 
     **/
    public function setData($data) {$this->data = $data; }
}

/**
 * Binary tree node.
 * Store the binary tree node.
 *
 * TODO: Find out how php actually stores arrays and reference objects in memory.
 * Refactor the code to optimize it, if there is a large memory overhead.
 * <This might not be the most optimal way to store the trees>
 **/
class BinaryTreeNode extends GenericNode{
    private $childNodes = array();
    CONST LEFT=0;
    CONST RIGHT=1;

    /**
     * Default Constructor.
     *
     * Creates an empty node without any children.
     **/
    public function __construct(){
        $this->childNodes [0]= null;  //left node
        $this->childNodes  [1]= null; //right node
    }

    /**
     * Constructs a new binary tree node with data.
     *
     * @param $data Any type of data used by the tree node. 
     **/
    public static function withData($data){
        $instance = new self();
        $instance->setData($data);
        return $instance;
    }

    /**
     * Add a new child node
     *
     * @param BinaryTreeNode $child Node to add
     * @param $pos 0, for left. 1, for right.
     **/
    public function addChild(BinaryTreeNode $child, $pos){
        $this->childNodes[$pos] = $child;
    }
    
    /**
     * Returns the a direct child of this node.
     *
     * @param $int Index of the child. Zero for left, One for right. 
     **/  
    public function getChild($int){
        if (is_int($int)){
            if($int < 0 || $int > $this->childCount())
                return null;
              return $this->childNodes[$int];
        }
        return null;
    }

    /**
     * Gets the immediate children in array format.
     * @return Array containing 2 elements. First element is for left node and second for right.
     */
    public function getChildren(){
        return $this->childNodes;
    }

    /**
     * Returns the number of immediate children this node have. To be fixed in future to get all the children of this node.
     *
     * @return Array Only 2 in current version.
     **/
    public function childCount(){
        return sizeof($this->childNodes);
    }

    /** 
     * Use this method for debug purposes
     */
    public function toString(){
         $childLeft = $this->getChild(0);
         $childRight = $this->getChild(1);

         $data = $this->getData();
         if(!$data)
             $data = "";
         
         $classname = "{$data} (" .get_class($this) . ")\n";
         $ret = "";
         if(!$childLeft)
             $ret .= "Left child is null\n";
         else
             $ret .= "Left child contains ". get_class($childLeft->getData()) . "\n"; 

         if(!$childRight)
             $ret .= "Right child is null\n";
         else
             $ret .= "Right child contains " . get_class($childRight->getData()) . "\n";
         
         return "\n\n". $classname . "{\n". $ret . "};\n\n";
        
    }
}

/**
 * BinaryTree class
 *
 * Use this class to manipulate the binary tree.
 *
 * @todo Add recursive tree search feature.
 * @todo Enforce BinaryTreeNode types as parameters. 
 **/
class BinaryTree{
    private $root;

    /**
     * Default Constructor
     **/
    public function __construct(){
    }

    /**
    * Construct a new Binary tree with Root node defined as an argument.
    * If the argument is not a BinaryTreeNode object then,  a new root node will be created
    * with the data supplied.
    * @param $root can either be a BinaryTreeNode or any other datatype.
    **/
    public static function withRoot($root){        
        $instance = new self();
        if ($root instanceof BinaryTreeNode)
            $instance->setRoot($root);
        else
            $instance->setRoot(BinaryTreeNode::withData($root));
        
        return $instance;
    }

    /** 
     * Add a new child to this binary tree's active node.
     * Active node is root by default.
     * @todo This feature needs to be properly implemented.
     * @param $child New node to add
     * @param $pos 0 - left, 1 - right
     **/
    public function addChild(BinaryTreeNode $child, $pos){
        if(!$this->root)
            $child = $this->root;
        parent::addChild($child, $pos);
    }

    /** 
     * Sets the root node.
     *
     * param Node to set as root.
     **/
    protected function setRoot($root){
        $this->root = $root;
    }

    /**
     * Search for and returns the node from the tree containing the given data.
     * Avoid using this method for large trees. Instead build the tree using the parent node from the database.
     */
    public function findNode($data){
        $walker = new BackTracker();
        $searchFunc = function($node, $parent, $lvl) use ($data){
            if ($node->getData() === $data){
                return $node;
            }
        };
        $walker->setMethod('enter', $searchFunc);
        $walker->executeWalk($this->root);
        
        return $walker->getReturnValue('enter');       
    }

    /**
     * Searches the tree for a given node, and returns information about it.
     * @return An array containing information about the nodes.
     **/
    public function findNodeInfo($data){
        $walker = new BackTracker();
        $searchFunc = function($node, $parent, $lvl, $i) use ($data){
            if ($node->getData() === $data){
                $position = array("LEFT", "RIGHT");
                return array(
                    "level" => $lvl,
                    "parent" => $parent,
                    "position" => $position[$i]
                );
            }
        };
        $walker->setMethod('enter', $searchFunc);
        $walker->executeWalk($this->root);
        return $walker->getReturnValue('enter');
    }
    
    /**
     * Returns the tree in string format. Used during Unit testing and debugging.
     **/
    public function toString(){
        $ret = "\n";
        $node = $this->root;        

        //$ret .= var_dump($node->getData());
        $ret .= $this->dumpChildren($node);
        
        $ret .= "\n";
        return $ret;
    }

    /**
     * Recursive Backtracker used to get the variable dump from the children.
     * Used by toString() method.
     **/
    private function dumpChildren(BinaryTreeNode $node = null,  $lvl=0){
        if ($node == null)
            $node = $this;
        //        $ret = "\n ";
        $ret = "";
        if (gettype($node) ===  "object")
            $ret .= $node->getData()->getName();
        elseif(gettype($node) === "string")
            $ret .= $node;
        
        $child = $node->getChildren();
        
        $leftNode = $child[0];
        $rightNode = $child[1];

        $ret .="{\n";
        
        $lvl++;
        for($i=0; $i<$lvl; $i++)
            $ret .= " ";
        
        if ($leftNode)
            $ret .= $this->dumpChildren($leftNode, $lvl);

        $ret .= ",";
        
        if ($rightNode)
            $ret .= $this->dumpChildren($rightNode, $lvl);
        
        $ret .= "}\n ";
        $lvl --;
        
        return $ret;
    }

    /** 
     * Get the root node of this tree.
     *
     * @return BinaryTreeNode root node of this tree.
     **/
    public function getRoot() { return $this->root; }
}


/**
 * 
 **/
abstract class TreeWalker{
    const BEGIN_STATE = 'begin';
    const END_STATE   = 'end';
    const ENTER_STATE = 'enter';
    const EXIT_STATE  = 'exit';
    const WALK_STATE  = 'walk';
    protected $funcTable = array('null' => null,
                                 self::BEGIN_STATE => null,
                                 self::END_STATE   => null,
                                 self::ENTER_STATE => null,
                                 self::EXIT_STATE  => null,
                                 self::WALK_STATE  => null);
    protected $returnTable = array(self::ENTER_STATE => false,
                                   self::EXIT_STATE  =>  false,
                                   self::WALK_STATE  => false,
                                   self::BEGIN_STATE => false
    );
    abstract function executeWalk(BinaryTreeNode $node, $lvl=0, $parent=null);
}

/** 
 * Recursive backtracker 
 * Every tree walker have five states.
 * Setting a return value to enter method immediately terminates the walk.
 * Example: 
 * <code>
 *  <?php
 *    //Assume $node is a node in an existing BinaryTree, with string data
 *    $walker = new BackTracker();
 *    $walker('enter', function($n){
 *            echo var_dump($n->getData()) . "\n"; 
 *     });
 *    $walker->walkChildren($node);      
 *   ?>
 * </code>
 * Be careful, while using the parent argument. Parent argument will be null when for the root. 
 **/
class BackTracker extends TreeWalker{       
    /**
     * Execute the walk.
     * Runs the user defined functions during the walk.
     * Parent will be null for the root.
     * Index, extra parameter used to store the index position of the current node in their parent's child array.
     **/
    public function executeWalk(BinaryTreeNode $node, $lvl=0, $parent=null, $index = 0){
        $child = $node->getChildren();
        
        //To improve readability these are stored in local variables. Refactor them out necessary.
        $onEnter = $this->funcTable[self::ENTER_STATE];
        $onWalk  = $this->funcTable[self::WALK_STATE];
        $onExit  = $this->funcTable[self::EXIT_STATE];
        $onBegin = $this->funcTable[self::BEGIN_STATE];
        $onEnd   = $this->funcTable[self::END_STATE];

        if($parent == null && $onBegin)
            $this->returnTable[self::BEGIN_STATE] = $onBegin($node); 
        
        //Execute onEnter() user method            
        if($onEnter != null)
            $enterRet =  $onEnter($node, $parent, $level=$lvl, $index);
        //If onEnter() returned a value then terminate the walk
        if (isset($enterRet)){
            $this->returnTable[self::ENTER_STATE] = $enterRet;
            return true;// $this->returnTable[self::ENTER_STATE];
        }

        $child = $node->getChildren();  //Update child; to enable walking the node's added during the enter state

        
        if ($child != null){
            $lvl++;
            for($i=0; $i< sizeof($child); $i++){
                if($child[$i] != null){          
                    //Continue the walk
                    if($onWalk)
                        $walkRet  = $onWalk($node, $child[$i]);
                    $this->executeWalk($child[$i], $lvl, $node, $i);
                }
            }            
        }

        //Execute exit state
        if ($onExit)
            $exitRet = $onExit($node, $parent, $lvl);
        
        //$this->onExitNode($node, $lvl--);
        if (isset($exitRet))
            $this->returnTable[self::EXIT_STATE] = $exitRet;

        
        if($parent == null && $onEnd)
            $endRes = $onEnd($node);

        if (isset($endRes))
            return $endRes;
            
        return true;
    }

    /**
     * callable hint is valid only for PHP >= 5.4 
     **/
    public function setMethod($key, callable $func){
        $this->funcTable[$key] = $func;
    }

    /**
     * Retrieves the last value returned by a method
     **/
    public function getReturnValue($key){
        return $this->returnTable[$key];
    }
}

//} //end of namespace
?>