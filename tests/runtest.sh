#!/bin/bash

#run unit tests
if hash phpunit 2>/dev/null; then
    phpunit . --testdox-html "logfile.html"
else
    echo "PHP Unit not found. Ensure /bin/phpunit or /usr/bin/phpunit is installed"
fi
