<?php

use PHPUnit\Framework\TestCase;


include_once "../src/BinaryTreeFactory.php";
include_once "../src/DotGenerator.php";
require_once "../src/FixedTree.php";
require_once "../src/TreeMaker/FixedTreeLoader.php";

require_once('../src/config.php');
use BinaryTreeVisualizer\Config;    
use BinaryTreeVisualizer\DB_Connector\PDO_UserDatabase as Database;
use BinaryTreeVisualizer\DB_Connector\DatabaseConfigVar as DBConfig;
use BinaryTreeVisualizer\TreeGenerator\DefaultTreeGenerator as BTGen;
use BinaryTreeVisualizer\TreeGenerator\FixedTreeGenerator;
use BinaryTreeVisualizer\TreeGenerator\FixedBinaryTreeLoader as FTLoader;
use BinaryTreeVisualizer\DotConverter as DotML;



class BinaryTreeFactory_Test extends TestCase{
    private $testUserId = "1000";
    public function testCreateFromDatabase(){
        
        $db = new Database(DBConfig::quickCfg(Config::$db_config));

        try{
            $btGen = new BTGen($db);
            $tree = $btGen->makeTree($this->testUserId);
            
            if($tree)
                echo ($tree->toString());
            
            $dotOut = new DotML($tree);
            echo $dotOut->getString();
            
            $this->assertNotNull($db);
            $this->assertNotNull($btGen);
            $this->assertNotNull($dotOut);
        }catch(Exception $ex){
            $this->markTestIncomplete("Backward compatibility test: Create new tree using DB basic method skipped. Make sure to add the test user_id.");
        }
    }

    public function testFixedTree(){
        try{
            $db = new Database(DBConfig::quickCfg(Config::$db_config));
        
            $btGen = new FixedTreeGenerator($db);
            $tree = $btGen->makeTree();

            $dotOut = new DotML($tree);
            echo $dotOut->getString();     
        
            $this->assertNotNull($db);
            $this->assertNotNull($tree);
        }catch(Exception $ex){
            $this->markTestIncomplete("Backward compatibility test: Create new tree using DB fixed v1.0 method skipped. Make sure to add the test user_id.");
        }
    }

    public function testNewFixedTree(){
        echo "\nTesting Tree v0.3..\n";
        $db = new Database(DBConfig::quickCfg(Config::$db_config));
        $btGen = new FTLoader($db);

        $cfg = new BinaryTreeVisualizer\TreeGenerator\TreeConfig();
        $cfg->rootUsrId = $this->testUserId;
        
        $tree = $btGen->makeTree($cfg);
        if($tree)
            echo $tree->toString();
        $dotOut = new DotML($tree);
        echo $dotOut->getString();
        $this->assertNotNull($db);
        $this->assertNotNull($tree);        
    }
    
}

?>