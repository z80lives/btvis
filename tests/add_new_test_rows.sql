TRUNCATE TABLE FixedTreeIndex;

INSERT INTO `FixedTreeIndex`
(       `user_key` ,
        `master_tree`,
	`position` 
)
VALUES
(10000, 0, 1),
(10001, 0, 2),
(10002, 0, 3),
(10003, 0, 4),
(10020, 1, 1),
(10021, 1, 2),
(10022, 1, 3)
;

