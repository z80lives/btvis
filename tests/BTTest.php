<?php
//namespace BinaryTreeVisualizer;

 use PHPUnit\Framework\TestCase;

 use BinaryTreeVisualizer\BinaryTreeNode;
 use BinaryTreeVisualizer\BackTracker;
/**
 * Binary tree class Test case
 **/

 include_once "../src/BinaryTree.class.php";

 class BinaryTree_Test extends TestCase{
     
     public function testConstruction(){

         $root = new BinaryTreeNode();
         $childLeft = new BinaryTreeNode();
         $childRight = new BinaryTreeNode();
         
         echo var_dump($root);
         echo $root->toString();

         echo "Adding left child to root\n";
         $root->addChild($childLeft, 0);
         echo "Adding right child to root\n";
         $root->addChild($childRight, 1);
         
         echo $root->toString();
         
         $this->assertNotNull($root);
         $this->assertNotNull($childLeft);
         $this->assertNotNull($childRight);
     }

     public function testRecursiveBacktracker(){
         $root = BinaryTreeNode::withData("Root");
         $this->addTestNodes($root);

         $walker = new BackTracker();
         echo "\n";

         //On Begin Method
         $walker->setMethod('begin', function(){
             echo "Starting the walk..\n";
         });
         
         //On End Method
         $walker->setMethod('end', function(){
             echo "\nWalk complete!\n";
         });

         //Enter method
         $walker->setMethod('enter', function($node, $parent, $lvl){
             if ($node->getData() === "Yeo"){
                 $a = $lvl;
                 //return  $a;
             }
         });         

         //Walk method
         $walker->setMethod('walk', function($node, $child){
             echo " {$node->getData()} -> {$child->getData()} : Walking \n ";
         }
         );

         //Exit method
         $walker->setMethod('exit', function($node, $parent, $lvl){
             static $deepest = 0;
             if($lvl > $deepest)
                 $deepest = $lvl;
             
             if ($parent)
                 echo " {$parent->getData()} <- {$node->getData()} : Backtracking \n";
             else
                 echo " {$node->getData()}";
             return $deepest;
         });
         
         $walker->executeWalk($root);
         $personAlvl = $walker->getReturnValue('enter');
         $exitRet = $walker->getReturnValue('exit');
             
         echo "\n {$personAlvl} \n  {$exitRet}\n";
         
         $this->assertNotNull($root);
         //$this->
         
     }

     public function testTreeSearchAndInfo(){
         $root = BinaryTreeNode::withData("Root");
         $this->addTestNodes($root);
         $tree = BinaryTreeVisualizer\BinaryTree::withRoot($root);
         //Search the tree for a node
         $userData = "Person A";        
         $node = $tree->findNode($userData);

         //Find the level of a given node
         $nodeInfo = $tree->findNodeInfo($userData);
         $nodeLevel = $nodeInfo['level'];

         $this->assertTrue($node != null);
         $this->assertTrue($nodeInfo != null);

         if($node){
             echo "\n". " Node: {$node->getData()} \n Level: {$nodeLevel} \n";
         }
     }

     private function addTestNodes($root){
         $root->addChild($b=BinaryTreeNode::withData("Ibrahim"), BinaryTreeNode::LEFT);
         $root->addChild($c=BinaryTreeNode::withData("Yeo"), BinaryTreeNode::RIGHT);
         
         $b->addChild($d=BinaryTreeNode::withData("Person A"), BinaryTreeNode::LEFT);
         $b->addChild($e=BinaryTreeNode::withData("Person B"), BinaryTreeNode::RIGHT);

         $c->addChild($f=BinaryTreeNode::withData("Person C"), BinaryTreeNode::LEFT);
         $c->addChild($g=BinaryTreeNode::withData("Person D"), BinaryTreeNode::RIGHT);         
     }
 }
?>