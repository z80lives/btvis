#!/usr/bin/perl
use strict;
use warnings;

print "-- Warning: Running this query will delete the existing user records from the database --\n";

print "-- Make sure SBC6 and SBC7 exists in the database before running the script --\n\n";

my $numRecord = 508;

my $userTableName = 'users';
my $clean = "TRUNCATE TABLE $userTableName;\n
             TRUNCAtE TABLE `personally_enrolled`; \n";  #Clear tables


my $first =  "INSERT INTO `$userTableName` (`user_id`, `user_name`, `user_email`, `user_pass`, `enroller_id`, `enrolled_id`, `direction`) VALUES";
$first .= q(
(11, 'talha', 'talha@gmail.com', '$2y$10$9qRNxzexeDu7b2konBDWVONlvUv0igd7/Xu5gFMRsQvv6mnNim6Ra', 'ROOT', 'SBC1', 'avail'),
(12, 'ibrahim', 'ibrahim@gmail.com', '$2y$10$cvIldExfp8L.5Z1vg0AqXOvz0OAFJWM2MW.9CIl74rdcSQCLf0LOO', 'SBC1', 'SBC2', 'left_mem'),
(13, 'mengtat', 'mengtat@gmail.com', '$2y$10$rVrjx3qR1NczlisTH1XbDueV/HYtNTNqyNQmH65PZk1/rUEaJKy1.', 'SBC1', 'SBC3', 'right_mem'); );

    
my $query = "INSERT INTO `$userTableName` (`user_id`, `user_name`, `user_email`, `user_pass`, `enroller_id`, `enrolled_id`, `direction`) VALUES ";

my $i = 10000;
my $name = "person_0";

print $clean;
print "\n\n";
print $first . "\n\n"; 
print $query;
print "\n\n";

# Expect the first two child nodes to be SBC6 and SBC 7
my @reserved_sbc = (2, 3 );

my $enroller_id = 2;

my $enrolled_id = 4;

my $lastNode = 0;

my $j = 0;
my $email = 'anon@server.com';
for(my $c=0; $c < $numRecord; $c++, $j++){
    #enroller id pattern
    if($j>1){
	$j = 0;
        $enroller_id ++;
    }
    
    #alternate between directions
    my $dir = 'right_mem';
    if($c % 2 == 0){
	$dir = 'left_mem';
    }
    
    my $id = $i+$c;
    $name = "person_$c";
    my $e_id = "SBC$enroller_id";
    my  $c_id = "SBC$enrolled_id";
    print "('$id', '$name', '$email', '', '$e_id', '$c_id', '$dir')";
    if( $c != $numRecord-1){
	print ", "
    }else{
	print ";";
    }
    
    print "\n";

    #get next enrolled_id
    if(grep $_ == $enrolled_id+1, @reserved_sbc){
	do{
	    $enrolled_id++
	}while(grep $_ == $enrolled_id, @reserved_sbc);
	
    }else{
	$enrolled_id++;
    }
   $lastNode = $enroller_id;
}

print ";\n";

# The right nodes seem to be 2n+1, where as the left node is 2n
print "INSERT INTO `personally_enrolled` (`enroller_id`, `left_mem`, `right_mem`) VALUES ";
for(my $c=1; $c <= $lastNode; $c++){
  my  $leftn = 2 * $c;
  my $rightn = $leftn + 1;
  my $node1 = "SBC$c";
  my $node2 = "SBC$leftn";
  my $node3 = "SBC$rightn";
  print "('$node1', '$node2', '$node3')";

    if( $c != $lastNode){
	print ", "
    }else{
	print ";";
    }

  print "\n";
}

