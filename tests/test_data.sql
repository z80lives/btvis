INSERT INTO `users` (`user_id`, `user_name`, `user_email`, `user_pass`, `enroller_id`, `enrolled_id`, `direction`) VALUES
(71, 'person1', 'anon@server.com', '', 'SBC6', 'SBC2', 'left_mem'),
(72, 'person2', 'anon@server.com', '', 'SBC6', 'SBC3', 'right_mem'),
(73, 'person3', 'anon@server.com', '', 'SBC7', 'SBC4', 'left_mem'),
(74, 'person4', 'anon@server.com', '', 'SBC7', 'SBC5', 'right_mem'),

(75, 'person5', 'anon@server.com', '', 'SBC2', 'SBC8', 'left_mem'),
(76, 'person6', 'anon@server.com', '', 'SBC2', 'SBC9', 'right_mem'),
(77, 'person7', 'anon@server.com', '', 'SBC3', 'SBC10', 'left_mem'),
(78, 'person8', 'anon@server.com', '', 'SBC4', 'SBC11', 'right_mem');


--- Update mapping table ----
-- Does the system does this automatically?
INSERT INTO `personally_enrolled` (`enroller_id`, `left_mem`, `right_mem`) VALUES
('SBC1', 'SBC6', 'SBC7'),
('SBC6', 'SBC2', 'SBC3'),
('SBC7', 'SBC4', 'SBC5'),
('SBC2', 'SBC8', 'SBC9'),
('SBC3', 'SBC10', ''),
('SBC4', '',  'SBC11')
;
