<?php
//namespace BinaryTreeVisualizer;

 use PHPUnit\Framework\TestCase;

use BinaryTreeVisualizer\BinaryTreeNode;
// use BinaryTreeVisualizer\BackTracker;
/**
 * Binary tree class Test case
 **/

include_once '../src/DatabaseWrapper.php';

include_once "../src/TreeMaker/FixedTreeLoader.php";


require_once ('../src/config.php');

 include_once "../src/TreeGraph.php";

use BinaryTreeVisualizer\DB_Connector\PDO_UserDatabase as Database;
use BinaryTreeVisualizer\DB_Connector\DatabaseConfigBuilder as DBConfigBuilder;
use BinaryTreeVisualizer\DB_Connector\DatabaseConfigVar as DefaultConfig;
use BinaryTreeVisualizer\DB_Connector\TestDatabaseConfig as TestConfig;
use BinaryTreeVisualizer\DB_Connector\UserRepository as Rep;
use BinaryTreeVisualizer\Config;    
use BinaryTreeVisualizer\TreeGenerator\FixedBinaryTreeLoader as FixedTreeGenerator;
use BinaryTreeVisualizer\TreeGenerator\TreeConfig;



use BinaryTreeVisualizer\TreeGraph;
     
 class TreeGraph_Test extends TestCase{
    public function testTreeGraphCreation(){
         $root = BinaryTreeNode::withData("Root");
         $this->addTestNodes($root);
         
         $tree = BinaryTreeVisualizer\BinaryTree::withRoot($root);
         $treeGraph = new TreeGraph($tree);
         $treeGraph->build();
         
         $nodes = $treeGraph->getNodes();
         $edges = $treeGraph->getEdges();
         //echo var_dump($treeGraph);         
         echo var_dump($nodes, $edges);
         //echo "\n";
         //echo $nodes[0]->getData();
         //echo $treeGraph->getChildren($nodes[0]);
         
         $this->assertNotNull($tree);

    }

     public function testTreeGraphDB(){
         $db = new Database(DefaultConfig::quickCfg(Config::$db_config));
        $btGen = new FixedTreeGenerator($db);

        $testUser = "31006";
        $treeConfig = new TreeConfig();

        $treeConfig->rootUsrId = $testUser;
        $tree = $btGen->makeTree($treeConfig);

        $treeGraph = TreeGraph::create($tree);

        $nodes = $treeGraph->getNodes();
        $edges = $treeGraph->getEdges();
        foreach($nodes as $node){
            $data = $node->getData();
            echo "{$data->getName()}: {$data->getTreePos()}\n";
        }
        echo "\n";
        foreach($edges as $edge){
            echo "({$edge[0]}, {$edge[1]}) => {$nodes[$edge[0]]->getData()->getTreePos()}, {$nodes[$edge[1]]->getData()->getTreePos()} }\n";
        }
        
     }
     
     private function addTestNodes($root){
         $root->addChild($b=BinaryTreeNode::withData("Ibrahim"), BinaryTreeNode::LEFT);
         $root->addChild($c=BinaryTreeNode::withData("Yeo"), BinaryTreeNode::RIGHT);
         
         $b->addChild($d=BinaryTreeNode::withData("Person A"), BinaryTreeNode::LEFT);
         $b->addChild($e=BinaryTreeNode::withData("Person B"), BinaryTreeNode::RIGHT);

         $c->addChild($f=BinaryTreeNode::withData("Person C"), BinaryTreeNode::LEFT);
         $c->addChild($g=BinaryTreeNode::withData("Person D"), BinaryTreeNode::RIGHT);         
     }
 }
?>