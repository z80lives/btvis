<?php
//namespace BinaryTreeVisualizer;

use PHPUnit\Framework\TestCase;

use BinaryTreeVisualizer\BinaryTreeNode;

/**
 * Binary tree class Test case
 **/

include_once "../src/Graph.class.php";
use BinaryTreeVisualizer\Graph;
use BinaryTreeVisualizer\GraphNode;


class Graph_Test extends TestCase{

     //Test graph class
     public function testSimpleGraph(){
         $graph = new Graph();
         $a = GraphNode::withData("A");
         $b = GraphNode::withData("B");
         $c = GraphNode::withData("C");
         $d = GraphNode::withData("D");
         
         $graph->addNode($a);
         $graph->addNode($b);
         $graph->addNode($c);
         $graph->addNode($d);

         $graph->addEdge($a, $b);
         $graph->addEdge($b, $c);
         $graph->addEdge($c, $a);
         
         $graph->removeNode($d);  
         $graph->removeEdge($c, $b);
         
         echo var_dump($graph);
       
         echo "\n -- ". $graph->count() . " -- \n";
         $this->assertNotNull($graph);
         $this->assertTrue($graph->isNeighbour($a, $c));
         $this->assertTrue($graph->isNeighbour($a, $b));
         $this->assertFalse($graph->isNeighbour($b,$c));
         $this->assertEquals($a, $graph->getNode($a));
     }

 }
?>