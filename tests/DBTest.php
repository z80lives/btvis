
<?php

include_once '../src/DatabaseWrapper.php';
require_once ('../src/config.php');

use PHPUnit\Framework\TestCase;
use BinaryTreeVisualizer\DB_Connector\PDO_UserDatabase as Database;
use BinaryTreeVisualizer\DB_Connector\DatabaseConfigBuilder as DBConfigBuilder;
use BinaryTreeVisualizer\DB_Connector\DatabaseConfigVar as DefaultConfig;
use BinaryTreeVisualizer\DB_Connector\TestDatabaseConfig as TestConfig;
use BinaryTreeVisualizer\DB_Connector\UserRepository as Rep;

use BinaryTreeVisualizer\Config;

class Database_Test extends TestCase{
    //    private $db_config = $db_config;// $GLOBALS['db_config'];
    private $db_config;
    private $testUser = "31006";

    protected function setup(){
        //$this->db_config = parse_ini_file("../config.ini");
        $this->db_config = Config::$db_config;
    }
    
    /**
     * Test basic ability to connect to the database.
     **/
    public function testBasicConnection(){
        // $this->db_config = parse_ini_file("../config.ini");       
                         
        $test_table = "personally_enrolled";
        $col_enroller_id   = "enroller_ID";
        $col_enrolled_id   = "enrolled_ID";
        $col_uname  = "user_Name";

        $user_table = "users";

        //        echo "DB_CONFIG", var_dump($test->db_config);
        $db = new Database(DefaultConfig::quickCfg($this->db_config));
        
        $db->query("SELECT * FROM ".$user_table);
        //        $db->bind(":users", $user_table);
        $result = $db->results();

        //   if($result)
        //    echo var_dump($result);

        foreach($result as $row){
            echo $row[$col_enrolled_id] ."  " . $row[$col_enroller_id] . " " . $row[$col_uname] . "\n";
        }
        
        $this->assertNotNull($db);

    }

    
    /**
     * Test the primary table from which we build the user interace
     **/
    public function testTables(){
        echo "\nTesting core database access...\n";
        
        $test_table = "personally_enrolled";
        $col_enroller_id   = "enroller_id";
        $col_leftmem       = "left_mem";
        $col_rightmem      = "right_mem";

        $user_table = "users";
        
        //        $db = new Database('shath', '123', 'mlm_database');
        $db = new Database(DefaultConfig::quickCfg($this->db_config));

        $db->query("SELECT * FROM ".$test_table);
        $result = $db->results();

        //   if($result)
        //    echo var_dump($result);

        foreach($result as $row){
            $this->assertNotNull($row[$col_enroller_id]);
            $this->assertNotNull($row[$col_leftmem]);
            $this->assertNotNull($row[$col_rightmem]);
                        
            echo $row[$col_enroller_id] ."  " . $row[$col_leftmem] . " " . $row[$col_rightmem] . "\n";
        }
        
        $this->assertNotNull($db);
    }

    /**
     * DB Config builder tests
     **/
    public function testConfigBuilder(){
        echo "\nTesting database configuration classes\n";
        $builder = new DBConfigBuilder();
        $director = new TestConfig($builder);
        $director->build();
        
        var_dump($builder);
        $this->assertNotNull($builder);
    }


    
    /** 
     * Test the repository class, (deprecrated)
     * test for tree v1
     * TODO REWRITE CLASS AND TEST FOR TREE V3
     */
    /*
    public function testRepositoryClass(){
        echo "\nTesting Repository class\n";
        //        $db = new Database('shath', '123', 'mlm_database');
        $db = new Database(DefaultConfig::quickCfg($this->db_config));

        $rep = new Rep($db);
        
        echo var_dump($rep);

        //Get the user mapping table from database and store it in a assoc array
        $userMap = $rep->getUserMap();    
        echo "\nUserMap: \n";
        echo var_dump($userMap);

        /*
        echo "\n Root Node =". $rep->getUserID("SBC6")."\n";

        $leftNode = $rep->getUserID("SBC6");
        
        
        $usr = $rep->getUser($leftNode);

        echo $usr->getName() . "\n";
        echo $usr->getEmail(). "\n";
        echo $rep->getPositionID($usr->getID()). "\n";
        **

        //echo var_dump($rep->getAllUserIds());

        //echo var_dump($rep->getChildren( $usr->getID() ));
       
        
        $this->assertNotNull($rep);
        $this->assertNotNull($userMap);
        $this->assertTrue($rep->countUsers() > 0);
    }*/

    /**
     * Test for Fixed BinaryTree hierarchy's index table 
     **/
    public function testFixedTreeRep(){
        $db = new Database(DefaultConfig::quickCfg($this->db_config));
        $rep = new Rep($db);

        $userId = "92";
        //$user = $rep->getUser($userId);

        /*
        echo var_dump($user);

        echo "\n";*/
        try{
            //$usr = $rep->getUser($this->testUser);
            $usr = $rep->getUser('92');
            
            //$fixedTree = $rep->getFixedTreeRecord(0, 1);
            $parent = $rep->getParentRecord($usr);
            //$parent2 = $rep->getParentRecord($usr->getId());

            echo var_dump($parent);

            
            /*
            $this->assertNotNull($rep->getFixedTreeRecord(0,0));        
            $this->assertNotNull($rep->getAllRootUsers());

            $this->assertNotNull($rep->getRootUser(1));
            $this->assertNotNull($rep->getParentRecord($usr));
            $this->assertNotNull($rep->getParentRecord($usr->getId()) );
        
            //$this->assertTrue($rep->isMasterUser($rep->getUser($userId)));
            $this->assertNotNull($db);
            $this->assertNotNull($rep);
            */

        }catch(\Exception $ex){
            echo "Test user not set: " . $ex->getMessage();
            $this->markTestIncomplete("Test skipped, test user not set.");

        }
        //$children = $rep->getChildren($usr->getId());

        //$rep->getTreeId('10000');
        //$rootUser = $rep->getRootUser(0);
        // echo "Testing fixed tree\n";
        // //
        // echo $rep->getPositionId('10000');
        //echo var_dump($rep->getAllRootUsers());

        //echo var_dump($rep->getUserMap(1));

        
        
    }

    public function testAdminFunctions(){
        $db = new Database(DefaultConfig::quickCfg($this->db_config));
        $rep = new Rep($db);

        try{
        $user = $rep->getUser('10508');
        $person1 = $rep->getUser('10006');
        $person2 = $rep->getUser('10008');
        $person3 = $rep->getUser('10010');

        if(!$rep->isMasterUser($user))
        {
            $rep->createNewRootUser($user);
            echo "User added to the tree!\n";
        }

        try{
            $rep->addUserToTree($person1, $user, $user);
        }catch(\Exception $ex){
            echo "\n Error occured : {$ex->getMessage()} \n";
        }
        //        $rep-
        //$rep->deleteRootUser($user);        
        
        $this->assertNotNull($db);
        $this->assertNotNull($user);
        $this->assertNotNull($rep);
        }catch(Exception $ex){
            echo "Test user not set!\n";
            $this->markTestIncomplete("Test skipped, test user not set.");            
        }
    }

    public function testProblem(){
        
        $db = new Database(DefaultConfig::quickCfg($this->db_config));
        $rep = new Rep($db);
        $yeo = $rep->getUser('31006');
        $usrId = '31006';
            
        if($rep->isMasterUser($yeo)){      
                
            $enrolled_id = "SBCx1";

            $user=null;
            $success=false;
            try{
                $user = $rep->getUser($usrId);
                $success = $rep->deleteRecord("users", "user_ID", "1000");
                //$rep->deleteUser();
                echo "\n";
                $success = $rep->removeUser($user);
                echo var_dump($success)."\n";
            }catch(\Exception $ex){
                echo "Error: {$ex->getMessage()}, usrId={$usrId}";
            }
            
        
            $success = $rep->addRecord("users",
                                       array(
                                           "user_ID" => $usrId,
                                           "user_Name" => "Test",
                                           "enrolled_ID" => $enrolled_id
                                       )
            );
            
            //$success = $rep->deleteRecord("users", "user_ID", "1000");
            $newUser = $rep->getUser($usrId);

            try{
                $success = $rep->addUserToTree($newUser, $yeo, $yeo);
            }catch(Exception $ex){
                echo "Error occured!";
            }
            
            echo var_dump($success);
            $this->assertTrue($success);

            //echo var_dump($yeo);
        }else{
            echo "{$yeo->getName()} is not in a tree.";
            $rep->createNewRootUser($yeo);
            echo "Created new root user.";

        }
    }
}

?>