<?php

use PHPUnit\Framework\TestCase;

include_once '../src/DatabaseWrapper.php';
require_once ('../src/config.php');


include_once "../src/VisJS.php";
include_once '../src/BinaryTree.class.php';
include_once '../src/TreeGraph.php';

include_once "../src/BinaryTreeFactory.php";
include_once "../src/DotGenerator.php";

include_once "../src/TreeMaker/FixedTreeLoader.php";

use BinaryTreeVisualizer\Graphics\VisJS;
use BinaryTreeVisualizer\BinaryTree;
use BinaryTreeVisualizer\BinaryTreeNode;
use BinaryTreeVisualizer\TreeGraph as TreeGraph;

use BinaryTreeVisualizer\DB_Connector\PDO_UserDatabase as Database;
use BinaryTreeVisualizer\DB_Connector\DatabaseConfigBuilder as DBConfigBuilder;
use BinaryTreeVisualizer\DB_Connector\DatabaseConfigVar as DefaultConfig;
use BinaryTreeVisualizer\DB_Connector\TestDatabaseConfig as TestConfig;
use BinaryTreeVisualizer\DB_Connector\UserRepository as Rep;

use BinaryTreeVisualizer\Config;    

use BinaryTreeVisualizer\TreeGenerator\DefaultTreeGenerator as BTGen;
use BinaryTreeVisualizer\TreeGenerator\FixedBinaryTreeLoader as FixedTreeGenerator;

use BinaryTreeVisualizer\TreeGenerator\TreeConfig;
use BinaryTreeVisualizer\DotConverter as DotML;


class TestVisJS extends TestCase{

    /*
    public function testTreeGraph(){
        //        $tree = new TreeGraph(BinaryTree::withRoot("Root"));
        $tree = new TreeGraph(BinaryTree::withRoot($root=BinaryTreeNode::withData("A")));

        $this->addTestNodes($root);
        
        $tree->build();

        echo "\n Nodes: ", var_dump($tree->getNodes());
        $this->assertNotNull($tree);
    }
        
    public function testHTMLGeneration(){
        $vis = new VisJS($btree=BinaryTree::withRoot("Root"));
        $this->addTestNodes($btree->getRoot());
        
        echo $vis->getScript();

        $testFile = "./~testViz.html";
        $handle = fopen($testFile,'w');
        fwrite($handle, $vis->getHTML());
        fclose($handle);
        

        $this->assertNotNull($vis);
    }
    
    public function testDBGeneration(){
        $db = new Database(DefaultConfig::quickCfg(Config::$db_config));
        $btGen = new BTGen($db);

        $treeConfig = new TreeConfig();
        //$treeConfig->fillEmptyNodes = false;
        //$treeConfig->rootPosId = "SBC81";
        
        $tree = $btGen->makeTree($treeConfig);
        $vis = new VisJS($tree);

        $vis->setObjectLabel('getName');
        
        

        $testFile = "./~testDB.html";
        $handle = fopen($testFile,'w');
        fwrite($handle, $vis->getHTML());
        fclose($handle);

        //echo $vis->getScript();
        //echo var_dump($vis->getRoot());
        $this->assertNotNull($vis);
    }
    */
    
    public function testVisWithFixedTree(){
        $db = new Database(DefaultConfig::quickCfg(Config::$db_config));
        $btGen = new FixedTreeGenerator($db);

        $testUser = "31006";
        $treeConfig = new TreeConfig();
        //$treeConfig->fillEmptyNodes = false;
        //$treeConfig->rootPosId = "SBC81";
        
        $treeConfig->rootUsrId = $testUser;
        $tree = $btGen->makeTree($treeConfig);
        $vis = new VisJS($tree);

        $vis->setObjectLabel('getName');                

        $testFile = "./~testDBFixed.html";
        $handle = fopen($testFile,'w');        
        fwrite($handle, $vis->getHTML());
        fclose($handle);

        $dot = new DotML($tree);
        echo $dot->getString();
        //echo $vis->getScript();
        //echo var_dump($vis->getRoot());
        $this->assertNotNull($vis);
    
    }    

    private function addTestNodes($root){
        $root->addChild($b=BinaryTreeNode::withData("Ibrahim"), BinaryTreeNode::LEFT);
        $root->addChild($c=BinaryTreeNode::withData("Yeo"), BinaryTreeNode::RIGHT);

        $b->addChild($d=BinaryTreeNode::withData("Person A"), BinaryTreeNode::LEFT);
        $b->addChild($e=BinaryTreeNode::withData("Person B"), BinaryTreeNode::RIGHT);

        $c->addChild($f=BinaryTreeNode::withData("Person C"), BinaryTreeNode::LEFT);
        $c->addChild($g=BinaryTreeNode::withData("Person D"), BinaryTreeNode::RIGHT);

    }
    /*
    private function addTestUserNodes(){
        $root->addChild($b=BinaryTreeNode::withData(new User), BinaryTreeNode::LEFT);
        $root->addChild($c=BinaryTreeNode::withData("Yeo"), BinaryTreeNode::RIGHT);

        $b->addChild($d=BinaryTreeNode::withData("Person A"), BinaryTreeNode::LEFT);
        $b->addChild($e=BinaryTreeNode::withData("Person B"), BinaryTreeNode::RIGHT);

        $c->addChild($f=BinaryTreeNode::withData("Person C"), BinaryTreeNode::LEFT);
        $c->addChild($g=BinaryTreeNode::withData("Person D"), BinaryTreeNode::RIGHT);
        }
        }*/
}
?>