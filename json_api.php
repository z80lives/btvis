<?php
/**
 * Handles Responses requested by the tree code.
 **/

//include_once('header.php');
include_once "src/BTVisualizer.php";
require_once ('src/config.php');

use BinaryTreeVisualizer\TreeGenerator\TreeConfig as TreeConfig;
use BinaryTreeVisualizer\DB_Connector\PDO_UserDatabase as Database;

use BinaryTreeVisualizer\DB_Connector\DatabaseConfigVar as DefaultConfig;


use BinaryTreeVisualizer\Config;

if(!isset($_POST['usr'])){
    echo "{title: 'error',
      msg: 'no data'
   }";
    exit(1);
}
$userId = $_POST['usr'];
$sponsorId = $_POST['sponsor_id'];
$formInfo = $_POST['formInfo'];
$parentInfo = $_POST['parentInfo'];
$alignInfo  = $_POST['ownInfo']['align'];

$treeConfig = new TreeConfig();
$treeConfig->rootUsrId = $userId;

$qv = null;
$rep = null;

try{
    $qv = new QuickVisualizer($treeConfig);
    $rep = $qv->getRepository();
    $userRecord = $rep->getUser($userId);
}catch(Exception $ex)
{
    echo <<<EOL
    {
     title: "Error:",
     msg: "Problem with the binary tree system. Please contact the administrator."
    }
EOL;
}

//Just substitute the user_id for now.
function make_enr_id($id){
    $k = "";   
    $f = array( '0', '3', '4', 'd', 'e',
                       '5', 'g', 'h', 'i', 'j',
                       '6', 'l', 'm', 'n', '4',
                'X', 'Z', '6', '8', '7', '9',
                '1', '2'
                       );
    for($i=0; $i<strlen($id); $i++){
        $k .= $f[$id[$i]];
    }
    return substr(str_shuffle("0123456789"), 0, 2).$k;
}

if(isset($userRecord))
{

function gen_uid($l=10){
    return substr(str_shuffle("0123456789"), 0, $l);
}
                   
function make_seed()
{
  list($usec, $sec) = explode(' ', microtime());
  return $sec + $usec * 1000000;
}

    $parent = $rep->getUser($parentInfo['id']);
    $sponsor = $rep->getUser($sponsorId);

    srand(make_seed());
    $uid =  crypt(gen_uid(7), rand(0, 400));//uniqid();
    $name = $formInfo['user_name'];
    $email = $formInfo['email'];
    
    define('HASH', '$2y$10$FW5I9L0JPbM7yI.zv00iFeukmBiTXz27BLNJwlXAfLKgzL6i3Wphi');
    $constant = "constant";
    $pass = "{$constant('HASH')}";
    $enroller_id = $sponsor->getBTId();
    
    if($enroller_id == "")
        $enroller_id = "orphan";
 
    srand(make_seed());
    //$enrolled_id = substr(uniqid("SBC", rand(0, 400) ), 0, 7);
    $enrolled_id = "SBC". make_enr_id($parent->getId());    
    $direction = $alignInfo;
    
    if ($direction == "R")
        $direction = "right_mem";
    else
        $direction = "left_mem";
    
    /*
  //Use this if repository method fails
  $query = "INSERT INTO users (`user_id`, `user_name`, `user_pass`, `enroller_id`, `enrolled_id`, `direction`)
      VALUES
     ('{$uid}',
      '{$name}',
      '{$pass}',
      '{$enroller_id}',
      '{$enrolled_id}',
      '{$direction}'
      );"; */

       $result = $rep->addRecord('users',
                    array(
                        'user_ID' => $uid,
                        'user_Name' => $name,
                        'user_Pass' => $pass,
                        'user_Email' => $email,
                        'enroller_ID' => $enroller_id,
                        'enrolled_ID'  => $enrolled_id,
                        'direction'  => $direction
                    )                    
                    );
    
      //echo $query;
       //$result = false; $user = null;

    /*$db = new Database(DefaultConfig::quickCfg(Config::$db_config));
       $db->query($query);
       $results = $db->results();
       $result = $db->success();
    */
    if($result){
        $userId = $uid;
        
        try{
            $user = $rep->getUser($uid);           
            $result = $rep->addUserToTree($user, $sponsor, $parent);
        }catch(\Exception $ex){
            $result = array("result" => 'fail',
                            "title" => "Sorry.",
                            "msg" => "Cannot add user to the tree", 'detail' => $ex->getMessage(),
                            'pid' => $sponsor->getName()
            );
            echo json_encode($result);
            $rep->deleteRecord('users', 'user_id', $uid);
            exit(1);
        }
        
    }else{
        $rep->deleteRecord('users', 'user_ID', $uid);
    }

    $info = null;
    if($user){
        $info = array('id' => $user->getId(),
                      'name' => $user->getName(),
                      'email' => $user->getEmail(),
                      'parent' => $parent->getName(),
                      'enrollerId' => $user->getEnrollerId(),
                      'enrolledId' => $user->getBTId()
        );
    }
    
    $result = array(
        'result' => $result,
        'uid'  => $uid,
        'info' => $info,
        'sponsor_id' => $sponsor->getId(),
         'enrolled_id' => $enrolled_id
        , 'enroller_id' => $enroller_id
        //,'query' => $query
    );

   echo json_encode($result);
}

?>